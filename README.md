# Team Based Application

## Assignment Overview

The aim of the assignment is to develop an application within a team using the [Agile development][1] approach. Each team will typically comprise of two or three students. The application should be written in the [Java programming language][8]. More specifically your application must be developed using [Test Driven Development (TDD)][2] with the [JUnit testing framework][3]. The developed code must be of high quality and should reflect industrial strength practices such as the use of:

* [Javadoc][4]
* [Coding standards][5]
* [Annotations (Java 5)][6]
* [UML class models][7]

The application must be built within the [Eclipse IDE][9].

## The Application

The overall purpose of the application is to provide a tool that can be used to aid the software development process within an organization. Essentially the product should be a "Software Development Accounting Framework (A tool support for [Agile SE][10])". **In some aspects the requirements are deliberately vague in order to simulate a real-life scenario, in which requirements are not always made fully clear by the client**.

Eventually the developed system may be used as the back-end to several different applications, including a **Web-based application** and a **Stand-alone desktop application**. Hence, the developed system **does not** require a **sophisticated graphical user interface (GUI)**. However there must be at least a basic (possibly text-based) interface which is capable of driving the main functionality of the application. Since this basic interface is likely to be used during development however the system should be designed so that this interface can be easily replaced in the future. In the same respect the developed system does not necessarily need to persist data using a [Relational Database Management System (RDBMS)][11], but again system design should allow this in future versions.

The exact nature of the product must be specified as part of the development process. The list below should act as a prompt for the type of functionality that would typically be supported by such a system:

1. Requirements management. (adding, editing, removing or requirements).
2. Bug tracking (storing, reporting, fixing, tracing etc.)
3. Storing of code/design reviews.
4. Support for test plans.
5. General project documentation.
6. Job control and scheduling
* _...anything else that is appropriate_

You may wish to refer to the documentation for some common systems which offer similar functionality such as [Bugzilla][12] and [Sourceforge Enterprise Edition][13].

## Deliverables

Your **team** must produce the following deliverables for the developed system:

1. A specification of the system to be developed.
2. Any design artifacts (e.g. UML models) produced during the development process.
3. The source code for the product itself
4. The JUnit test code used during development of the product.

Once added as a project to the [Eclipse IDE][9] the source should easily compile and be testable.

## Marking Scheme

The assignment will be equally marked, unless at the demonstration it transpires that a different mark spilt is necessary.

| Mark     | Minimum Requirements     |
| :------------- |:------------|
| Fail: < 40%     | The provided code does not compile or the built product is not usable for its primary purpose (i.e. of managing artefacts related to the software development process). Not all of the stated deliverables are present.     |
| Pass: 40%+     | An application has been developed that provides some appropriate functionality. The development process however was not necessarily clear and there is limited evidence that an Agile approach was taken. Most of the stated deliverables are present, but limited. |
| Merit: 60%+     | The produced application is good enough to be used to support software development projects. A number of useful features are present and it is clear that the product was developed using an Agile approach. All of the stated deliverables are present including a clear specification statement.     |
| Distinction: 70%+     | The application is generally commercial strength. All deliverables are provided and accurately reflect and evidence the Agile approach taken. The JUnit testing code, javadoc and use of annotations is also of commercial grade.      |


[1]: https://en.wikipedia.org/wiki/Agile_software_development
[2]: http://agiledata.org/essays/tdd.html
[3]: http://www.tutorialspoint.com/junit/junit_test_framework.htm
[4]: http://www.oracle.com/technetwork/articles/java/index-137868.html
[5]: http://www.nws.noaa.gov/oh/hrl/developers_docs/General_Software_Standards.pdf
[6]: http://www.javabeat.net/annotations-in-java-5-0/
[7]: http://www.tutorialspoint.com/uml/index.htm
[8]: http://www.tutorialspoint.com/java/index.htm
[9]: http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/indigosr2
[10]: http://www.incose.org/ChaptersGroups/WorkingGroups/knowledge/agile-systems-se
[11]: https://en.wikipedia.org/wiki/Relational_database_management_system
[12]: https://bugzilla.mozilla.org/
[13]: http://help.collab.net/index.jsp?topic=/rnotes/sfee440.html