package testcase;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import engine.Core;

public class IntToStingTest {

	@Test
	public void IntToString_Test() {
		List<String> result = Core.number_to_string(10, 0, false);
		assertEquals(Arrays.asList("0","1","2","3","4","5","6","7","8","9","10"), result);
	}

}
