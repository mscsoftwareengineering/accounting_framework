package testcase;

import static org.junit.Assert.*;

import org.junit.Test;

import engine.Text_Manager;

public class StylesheetTest {

	@Test
	public void Stylesheet_Test() {
		String result = Text_Manager.read_stylesheet("Classic", "QSpinBox");
		assertEquals("QSpinBox { background: #ffffff; } ", result);
	}

}
