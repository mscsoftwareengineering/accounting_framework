package testcase;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.junit.Test;

import engine.Core;

public class IntToStringTest_Exterime {

	@Test
	public void IntToString_Exterime_Test() {
		Random rand = new Random();
		int  number1 = rand.nextInt(50) + 1;
		int  number2 = rand.nextInt(50) + 1;
		List<String> result = Core.number_to_string(number1, number2, false);
		assertEquals(Math.abs(number1-number2)+1, result.size());
	}

}
