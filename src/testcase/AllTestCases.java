package testcase;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Database_Create_Test.class, IntToStingTest.class, IntToStringTest_Exterime.class,
		StylesheetTest.class })
public class AllTestCases {

}
