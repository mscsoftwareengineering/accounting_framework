package engine;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.trolltech.qt.gui.QTreeWidgetItem;

public class SQLite_Manager {
	Connection connection = null;
	public static Connection database_connection(String file_name)
	{
		try 
		{
			Class.forName("org.sqlite.JDBC");
			String path = new File("src/data/"+file_name+".sqlite").getAbsolutePath().replace("\\","/") ;
			Connection connection =  DriverManager.getConnection("jdbc:sqlite:" + path);
			System.out.println("Connection Successful");
			return connection;
		} 
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return null;
		}
	}
	
	/**
	 * 
	 * reserved table required:
	 * 1 - ID 
	 * 2 - Table Name 
	 * 3 - Customer Name
	 * 4 - Customer Number 
	 * 5 - Size 
	 * 6 - Time-in 
	 * 7 - Time-out 
	 * 8 - Date 
	 * 9 - Total 
	 * 10 - Status 
	 * 11 - Dish List 
	 * 12 - Dish Price 
	 * 13 - Drink List 
	 * 14 - Drink Price
	 * 
	 * @param table_name
	 * @param person_name
	 * @param person_number
	 * @param person
	 * @param time
	 * @param date
	 * @param total
	 */
	public static void add_reservation(String table_name, String customer_name, String customer_number, String size, String time, String date, String total)
	{
		try {
			Connection connection = database_connection("reservation");
			PreparedStatement statement = connection.prepareStatement("INSERT INTO reserved VALUES($next_id,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			// {"ID" ,"Table Number", "Name", "Contact_Number", "Person", "Time-in", "Date", "Total"}
			statement.setString(2,table_name);
			statement.setString(3,customer_name);
			statement.setString(4,customer_number);
			statement.setString(5,size);
			statement.setString(6,time);
			statement.setString(7,null);
			statement.setString(8,date);
			statement.setString(9,total);
			statement.setString(10,"Active");
			statement.setString(11,null);
			statement.setString(12,null);
			statement.setString(13,null);
			statement.setString(14,null);
			statement.executeUpdate();
			
			connection.setAutoCommit(true);
			connection.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	/**
	 * 
	 * @param order_number
	 * @param customer_name
	 * @param customer_number
	 * @param delivery_type
	 * @param area
	 * @param street
	 * @param pobox
	 * @param note
	 * @param time_in
	 * @param time_out
	 * @param date
	 * @param total
	 * @param status
	 * @param meal_list
	 * @param meal_price
	 * @param drink_list
	 * @param drink_price
	 */
	public static void add_order(String order_number, String customer_name, String customer_number, 
								 String delivery_type, String area, String street, String pobox,
								 String note, String time_in, String time_out, String date, String total, String status,
								 String meal_list,String meal_price,String drink_list,String drink_price)
	{
		try {
			Connection connection = database_connection("order");
			PreparedStatement statement = connection.prepareStatement("INSERT INTO ordered VALUES($next_id,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			//
			statement.setString(2,order_number);
			statement.setString(3,customer_name);
			statement.setString(4,customer_number);
			statement.setString(5,delivery_type);
			statement.setString(6,area);
			statement.setString(7,street);
			statement.setString(8,pobox);
			statement.setString(9,note);
			statement.setString(10,time_in);
			statement.setString(11,time_out);
			statement.setString(12,date);
			statement.setString(13,total);
			statement.setString(14,status);
			statement.setString(15,meal_list);
			statement.setString(16,meal_price);
			statement.setString(17,drink_list);
			statement.setString(18,drink_price);
			statement.executeUpdate();
			
			connection.setAutoCommit(true);
			connection.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public static List<QTreeWidgetItem> read_all(String filter)
	{
		List<QTreeWidgetItem> ListItems = new ArrayList<QTreeWidgetItem>();
		try {
			Connection connection = database_connection("reservation");
			 Statement statement = connection.createStatement();
//			PreparedStatement statement = connection.prepareStatement("SELECT * FROM reserved;");
			ResultSet result = statement.executeQuery("select * from reserved;");
			while (result.next()) {
				if (result.getString("Status").equals(filter))
				{
					QTreeWidgetItem item = new QTreeWidgetItem();
					// {"ID" ,"Table Number", "Name", "Contact Number", "Person", "Time-in", "Date", "Total"}
					item.setText(0, result.getString("Table Name"));
					item.setText(1, result.getString("Customer Name"));
					item.setText(2, result.getString("Customer Number"));
					item.setText(3, result.getString("Size"));
					item.setText(4, result.getString("Time-in"));
					item.setText(5, result.getString("Date"));
					item.setText(6, result.getString("Total"));
					item.setText(7, result.getString("Status"));
					item.setText(8, result.getString("ID"));
					ListItems.add(item);
				}
	        }
			statement.close();
			result.close();
			
			return ListItems;
		} catch (Exception e) {
			System.out.println(e);
			return ListItems;
		}
	}
	/**
	 * getting list of ordered items
	 * ordered table's Titles:
	 * - ID
	 * - Order Number
	 * - Customer Name
	 * - Customer Number
	 * - Type
	 * - Area
	 * - Street
	 * - Postal Code
	 * - Note
	 * - Time-in
	 * - Time-out
	 * - Date
	 * - Total
	 * - Status
	 * - Meal List
	 * - Meal Price
	 * - Drink List
	 * - Drink Price
	 * 
	 * @return List of QTreeWidgetItem
	 */
	public static List<QTreeWidgetItem> query_order_database()
	{
		List<QTreeWidgetItem> ListItems = new ArrayList<QTreeWidgetItem>();
		try {
			Connection connection = database_connection("order");
			 Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("select * from ordered;");
			while (result.next()) {
				QTreeWidgetItem item = new QTreeWidgetItem();
				item.setText(0, result.getString("Status"));
				item.setText(1, result.getString("Order Number"));
				item.setText(2, result.getString("Customer Name"));
				item.setText(3, result.getString("Customer Number"));
				item.setText(4, result.getString("Type"));
				item.setText(5, result.getString("Time-in"));
				item.setText(6, result.getString("Time-out"));
				item.setText(7, result.getString("Total"));
				item.setText(8, result.getString("Area"));
				item.setText(9, result.getString("Street"));
				item.setText(10, result.getString("Postal Code"));
				item.setText(11, result.getString("Note"));
				item.setText(12, result.getString("Date"));
				item.setText(13, result.getString("Meal List"));
				item.setText(14, result.getString("Meal Price"));
				item.setText(15, result.getString("Drink List"));
				item.setText(16, result.getString("Drink Price"));
				item.setText(17, result.getString("ID"));
				ListItems.add(item);

	        }
			statement.close();
			result.close();
			
			return ListItems;
		} catch (Exception e) {
			System.out.println(e);
			return ListItems;
		}
	}
	
	
	/**
	 * getting list of Available menu based on given type for filter
	 * MenuList table's Titles:
	 * - ID
	 * - Type
	 * - Name
	 * - Rank
	 * - Time
	 * - Price
	 * - Description
	 * - Image
	 * @param filter
	 * @return
	 */
	public static List<QTreeWidgetItem> get_menu(String filter)
	{
		List<QTreeWidgetItem> ListItems = new ArrayList<QTreeWidgetItem>();
		try {
			Connection connection = database_connection("menu");
			 Statement statement = connection.createStatement();
//			PreparedStatement statement = connection.prepareStatement("SELECT * FROM reserved;");
			ResultSet result = statement.executeQuery("select * from MenuList;");
			while (result.next()) {
				if (result.getString("Type").equals(filter))
				{
					QTreeWidgetItem item = new QTreeWidgetItem();
					// {"ID" ,"Table Number", "Name", "Contact Number", "Person", "Time-in", "Date", "Total"}
					item.setText(0, result.getString("Name"));
					item.setText(1, result.getString("Time"));
					item.setText(2, result.getString("Price"));
					item.setText(3, result.getString("ID"));
					ListItems.add(item);
				}
	        }
			statement.close();
			result.close();
			
			return ListItems;
		} catch (Exception e) {
			System.out.println(e);
			return ListItems;
		}
	}

}
