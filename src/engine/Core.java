package engine;

import java.util.ArrayList;
import java.util.List;

public class Core
{
	/**
	 * Get list of strings from one number to another number 
	 * 
	 * @author Bernard Rouhi
	 * @param start_number number to start from
	 * @param end_number number to end with
	 * @param preview to view the result
	 * @return List<String> of all the numbers
	 */
	public static List<String> number_to_string(int start_number, int end_number, boolean preview)
	{
		int first_num, second_num;
		if (start_number > end_number)
		{
			first_num = end_number;
			second_num = start_number;
		}
		else
		{
			first_num = start_number;
			second_num = end_number;
		}
		List<String> sting_list = new ArrayList<String>(); 
		int number = first_num;
		while (number <= second_num)
		{
			sting_list.add(Integer.toString(number));
			number++;
		}
		if (preview)
		{
			System.out.println(sting_list);			
		}
		return sting_list;
	}
	
	public static double[] cost (int[] price_list, int gst_percent)
	{
		double total_cost = 0.0, total_gst = 0.0, total_spend=0.0;
		
		for (int num=0; num < price_list.length; num++)
		{
			total_spend += Math.abs(price_list[num]);
		}
		if (total_spend > 0.0)
		{
			total_gst = (6/100.0)*total_spend;
		}
		double[] final_list = {total_spend,total_gst, total_cost} ;
		return final_list;
		
	}
}