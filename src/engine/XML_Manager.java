package engine;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XML_Manager
{
	public static NodeList read_xml(String file_name, String group_name)throws Exception
	{
		String path = new File("src/data/"+file_name+".xml").getAbsolutePath().replace("\\","/") ;
		File file_path = new File(path);
		DocumentBuilderFactory document_builder_factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder document_builder = document_builder_factory.newDocumentBuilder();
		Document document = document_builder.parse(file_path);
		
		NodeList list = document.getElementsByTagName(group_name);
		
		return list;
	}
	
	public static void write_xml(String file_name, Document document) throws Exception
	{
		String path = new File("src/data/"+file_name+".xml").getAbsolutePath().replace("\\","/") ;
		File file_path = new File(path);
		
		TransformerFactory transformer_factory = TransformerFactory.newInstance();
		Transformer transformer = transformer_factory.newTransformer();
		DOMSource source = new DOMSource(document);
		
		StreamResult stream_result = new StreamResult(file_path);
		
		transformer.transform(source, stream_result);
	}
	
	public static Document list_string_to_document(String group_name, String group_value, String[] title_list, String[] data_list ) throws Exception
	{
		DocumentBuilderFactory document_builder_factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder document_builder = document_builder_factory.newDocumentBuilder();
		Document document = document_builder.newDocument();
		
		Element element = document.createElement(group_name);
		Attr attr = document.createAttribute("ID");
		attr.setValue(group_value);
		element.setAttributeNode(attr);
		
		for (int count=0;count<title_list.length;count++)
		{
			Element new_field = document.createElement(title_list[count]);
			new_field.appendChild(document.createTextNode(data_list[count]));
			element.appendChild(new_field);			
		}
		
		document.appendChild(element);
		
		return document;
	}
	
}