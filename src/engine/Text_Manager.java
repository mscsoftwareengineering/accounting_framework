package engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Text_Manager
{
	
	/**
	 * Reading the stylesheets within package by giving the Style name and 
	 * User Interface widget
	 * @author Bernard Rouhi
	 * @version 1.0
	 * @param style_group Name of the Style
	 * @param style_file Name of the widget
	 * @return String
	 */
	public static String read_stylesheet(String style_group, String style_file)
	
	{
		String text = "";
		try
		{
			String path = new File("src/styles/" + style_group+ "/"+ style_file +".txt").getAbsolutePath().replace("\\","/");
			Scanner file_txt = new Scanner(new File(path));
			
			while (file_txt.hasNextLine())
			{
				text = text + file_txt.next()+ " ";
			}
			file_txt.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Stylesheet Dosen't Exist");
		}
		
		return text;
	}
	
}

