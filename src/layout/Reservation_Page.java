package layout;

import com.trolltech.qt.gui.*;

import config.App_Setting;
import config.Database_Settings;

import com.trolltech.qt.core.*;
import engine.Text_Manager;
import engine.XML_Manager;
import engine.Core;
import engine.SQLite_Manager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;

public class Reservation_Page extends QWidget
{
	Date current_date = new Date();
	QLineEdit edit_table_name = new QLineEdit();
	QLineEdit edit_person_name = new QLineEdit();
	QLineEdit edit_person_number = new QLineEdit();
	QSpinBox spin_person = new QSpinBox();
	QComboBox combo_date_day = new QComboBox();
	QComboBox combo_date_month = new QComboBox();
	QComboBox combo_date_year = new QComboBox();
	
	List<String> year_list;
	List<String> month_list;
	List<String> day_list;
	public Reservation_Page()
	{
		setWindowTitle(String.format("Reserve v%1.2f",1.0));
        setMinimumHeight(210);
        setMinimumWidth(370);
        setGeometry(250, 250, 350, 100);
        setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QWidget"));
        
        initUI();
        
	}
	
	private void initUI() 
	{
		QVBoxLayout main_layout = new QVBoxLayout(this);
		main_layout.setContentsMargins(0,0,0,0);
		main_layout.setSpacing(0);
		
		QRegExp val_txt = new QRegExp("[a-zA-Z-0-9]+");
		QRegExp val_name = new QRegExp("[a-zA-Z]+[a-zA-Z\\s]+");
		QRegExp val_num = new QRegExp("[0-9]+");
		
		//-------------------NEW LAYOUT------------------
		QGridLayout pages_layout = new QGridLayout();
		pages_layout.setContentsMargins(5,5,5,5);
		pages_layout.setSpacing(2);
//		pages_layout.setAlignment(this, Qt.AlignmentFlag.AlignTop,Qt.AlignmentFlag.AlignRight);
		
		main_layout.addLayout(pages_layout);
		
		QLabel text_table_name = new QLabel("Table Number:");
		text_table_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		
		edit_table_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		QRegExpValidator table_validator = new QRegExpValidator(val_txt,edit_table_name);
		edit_table_name.setValidator(table_validator);
		
		QLabel text_person_name = new QLabel("Costomer Name:");
		text_person_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_person_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		QRegExpValidator person_name_validator = new QRegExpValidator(val_name,edit_person_name);
		edit_person_name.setValidator(person_name_validator);
		
		QLabel text_person_number = new QLabel("Costomer Number:");
		text_person_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_person_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		QRegExpValidator person_number_validator = new QRegExpValidator(val_num,edit_person_number);
		edit_person_number.setValidator(person_number_validator);
		
		QLabel text_person = new QLabel("Person:");
		text_person.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		spin_person.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QSpinBox"));
		spin_person.setRange(1, 10000);
		
		QLabel text_date = new QLabel("Date:");
		text_date.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		combo_date_day.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QComboBox"));
		combo_date_month.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QComboBox"));
		combo_date_year.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QComboBox"));
//		combo_date_day.addItems(texts);
		
		pages_layout.addWidget(text_table_name,0,0,Qt.AlignmentFlag.AlignCenter, Qt.AlignmentFlag.AlignRight);
		pages_layout.addWidget(edit_table_name,0,1,1,3);
		pages_layout.addWidget(text_person_name,1,0,Qt.AlignmentFlag.AlignCenter, Qt.AlignmentFlag.AlignRight);
		pages_layout.addWidget(edit_person_name,1,1,1,3);
		pages_layout.addWidget(text_person_number,2,0,Qt.AlignmentFlag.AlignCenter, Qt.AlignmentFlag.AlignRight);
		pages_layout.addWidget(edit_person_number,2,1,1,3);
		pages_layout.addWidget(text_person,3,0,Qt.AlignmentFlag.AlignCenter, Qt.AlignmentFlag.AlignRight);
		pages_layout.addWidget(spin_person,3,1,1,3);
		pages_layout.addWidget(text_date,4,0,Qt.AlignmentFlag.AlignCenter, Qt.AlignmentFlag.AlignRight);
		pages_layout.addWidget(combo_date_day,4,1);
		pages_layout.addWidget(combo_date_month,4,2);
		pages_layout.addWidget(combo_date_year,4,3);
		
		//-------------------NEW LAYOUT------------------
		QHBoxLayout pages_action = new QHBoxLayout();
		pages_action.setContentsMargins(5,5,5,5);
		pages_action.setSpacing(2);
		
		main_layout.addLayout(pages_action);
		
		QPushButton action_submit = new QPushButton("Submit");
		action_submit.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_submit.released.connect(this,"submit()");
		QPushButton action_clear = new QPushButton("Clear");
		action_clear.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_clear.released.connect(this,"clear()");
		
		pages_action.addWidget(action_submit);
		pages_action.addWidget(action_clear, 1, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignLeft);
		
		//-------------------SET DATE------------------
		set_date();
		combo_date_month.currentIndexChanged.connect(this,"update_date()");
		combo_date_year.currentIndexChanged.connect(this,"update_date()");
		
	}
	/**
	 * Submit the information to database
	 * @throws Exception 
	 */
	private void submit() throws Exception
	{
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simple_format= new SimpleDateFormat("HH:mm:ss");
        
		String submite_date = combo_date_year.currentText()+"/"+combo_date_month.currentText()+"/"+combo_date_day.currentText();
		// Submit to Database
		SQLite_Manager.add_reservation(
				edit_table_name.text(),
				edit_person_name.text(),
				edit_person_number.text(),
				spin_person.text(),
				simple_format.format(calendar.getTime()),
				submite_date, 
				"0");
		close();
		clear();
	}
	/**
	 * reset the fields
	 */
	private void clear()
	{
		
		edit_table_name.setText("");
		edit_person_name.setText("");
		edit_person_number.setText("");
		spin_person.setValue(1);
		set_date();
	}
	
	/**
	 * set the current date and set the Years, months, and Days for ComboBox
	 */
	private void set_date()
	{
		year_list = Core.number_to_string(QDate.currentDate().year(),QDate.currentDate().year()+5,false);
		combo_date_year.insertItems(0, year_list);
		combo_date_year.setCurrentIndex(combo_date_year.findText(Integer.toString(QDate.currentDate().year())));
		
		month_list = Core.number_to_string(1,12,false);
		combo_date_month.insertItems(0, month_list);
		combo_date_month.setCurrentIndex(combo_date_month.findText(Integer.toString(QDate.currentDate().month())));
		
		day_list = Core.number_to_string(1,QDate.currentDate().daysInMonth(),false);
		combo_date_day.insertItems(0, day_list);
		combo_date_day.setCurrentIndex(combo_date_day.findText(Integer.toString(QDate.currentDate().day())));
	}
	
	/**
	 * Update the date and set the days base on selected Month and Year
	 */
	private void update_date()
	{
		int new_year, new_month; 
		new_year = Integer.parseInt(combo_date_year.currentText().toString());
		new_month = Integer.parseInt(combo_date_month.currentText().toString());
		QDate new_date = new QDate(new_year,new_month,1);
		
		//Updating the total days in month
		day_list = Core.number_to_string(1,new_date.clone().daysInMonth(),false);
		combo_date_day.clear();
		combo_date_day.insertItems(0, day_list);
	}
}