package layout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.trolltech.qt.core.QDate;
import com.trolltech.qt.core.QRegExp;
import com.trolltech.qt.core.Qt;
import com.trolltech.qt.core.Qt.MatchFlags;
import com.trolltech.qt.gui.*;

import config.App_Setting;
import engine.SQLite_Manager;
import engine.Text_Manager;

/**
 * This page is to set order for delivery or take away
 * 
 * @author Bernard Rouhi
 *
 */
public class Order_Page extends QWidget
{
	QComboBox menu_items = new QComboBox();
	QTreeWidgetItem menu_header = new QTreeWidgetItem();
	QTreeWidget menu_list = new QTreeWidget();
	QTreeWidget selection_list = new QTreeWidget();
	QLineEdit edit_order = new QLineEdit();
	QLineEdit edit_name = new QLineEdit();
	QLineEdit edit_number = new QLineEdit();
	QComboBox combo_deliver_by = new QComboBox();
	QLineEdit edit_area = new QLineEdit();
	QLineEdit edit_street = new QLineEdit();
	QLineEdit edit_pobox = new QLineEdit();
	QTextEdit textedit_note = new QTextEdit();
	QLabel dynamic_time = new QLabel("Enter");
	QLabel dynamic_cost = new QLabel("Enter");
	QLabel dynamic_gst = new QLabel("Enter");
	QLabel dynamic_total = new QLabel("Enter");
	
	public Order_Page()
	{
		setWindowTitle(String.format("Order v%1.2f",1.0));
        setMinimumHeight(530);
        setMinimumWidth(700);
        setGeometry(250, 250, 350, 100);
        setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QWidget"));
        
        initUI();

	}
	private void initUI() 
	{
		QHBoxLayout main_layout = new QHBoxLayout(this);
		main_layout.setContentsMargins(5,5,5,5);
		main_layout.setSpacing(0);
		
		QRegExp val_name = new QRegExp("[a-zA-Z]+[a-zA-Z\\s]+");
		QRegExp val_num = new QRegExp("[0-9]+");
		
		//-------------------NEW LAYOUT------------------
		QVBoxLayout selection_layout = new QVBoxLayout();
		selection_layout.setContentsMargins(5,5,5,5);
		selection_layout.setSpacing(2);
		
		main_layout.addLayout(selection_layout);
		
		menu_items.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QComboBox"));
		menu_items.addItem("Meal");
		menu_items.addItem("Drink");
		menu_items.currentIndexChanged.connect(this,"update_menu()");
		
		selection_layout.addWidget(menu_items);
		//-------------------NEW LAYOUT------------------
		QHBoxLayout menu_list_layout = new QHBoxLayout();
		menu_list_layout.setContentsMargins(5,5,5,5);
		menu_list_layout.setSpacing(2);
		
		selection_layout.addLayout(menu_list_layout);
		
		
		menu_list.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		menu_header.setText(0, "Name");
		menu_header.setText(1, "Time");
		menu_header.setText(2, "Price");
		menu_list.setHeaderItem(menu_header);
		
		QPushButton add_item = new QPushButton("+");
		add_item.released.connect(this,"add_selected()");
		add_item.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Quick_Action_Button"));
		
		menu_list_layout.addWidget(menu_list);
		menu_list_layout.addWidget(add_item,1,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignTop);
		
		//-------------------NEW LAYOUT------------------
		QHBoxLayout selection_list_layout = new QHBoxLayout();
		selection_list_layout.setContentsMargins(5,5,5,5);
		selection_list_layout.setSpacing(2);
		
		selection_layout.addLayout(selection_list_layout);
		
		selection_list.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		selection_list.setHeaderItem(menu_header);
		
		QPushButton remove_item = new QPushButton("-");
		remove_item.released.connect(this,"remove_selected()");
		remove_item.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Quick_Action_Button"));
		
		selection_list_layout.addWidget(selection_list);
		selection_list_layout.addWidget(remove_item,1,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignTop);
		
		//-------------------NEW LAYOUT------------------
		QVBoxLayout fields_layout = new QVBoxLayout();
		fields_layout.setContentsMargins(5,5,5,5);
		fields_layout.setSpacing(2);
		
		main_layout.addLayout(fields_layout);
		
		//-------------------NEW LAYOUT------------------
		QGridLayout info_layout = new QGridLayout();
		info_layout.setContentsMargins(5,5,5,5);
		info_layout.setSpacing(2);
		
		fields_layout.addLayout(info_layout);
		
		QLabel text_order = new QLabel("Order Number:");
		text_order.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_order.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		
		QLabel text_name = new QLabel("Customer Name:");
		text_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		QRegExpValidator name_validator = new QRegExpValidator(val_name,edit_name);
		edit_name.setValidator(name_validator);
		
		QLabel text_number = new QLabel("Customer Number:");
		text_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		QRegExpValidator number_validator = new QRegExpValidator(val_num,edit_number);
		edit_number.setValidator(number_validator);
		
		QLabel text_deliver_by = new QLabel("Deliver By:");
		text_deliver_by.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		combo_deliver_by.addItem("Takeaway");
		combo_deliver_by.addItem("Motorbike");
		combo_deliver_by.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QComboBox"));
		
		QLabel text_area = new QLabel("Area:");
		text_area.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_area.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		
		QLabel text_street = new QLabel("Street:");
		text_street.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_street.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		
		QLabel text_pobox = new QLabel("Postal Code:");
		text_pobox.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		edit_pobox.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLineEdit"));
		
		QLabel text_note = new QLabel("Note:");
		text_note.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		textedit_note.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTextEdit"));
		
		QLabel text_time = new QLabel("Time:");
		text_time.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_time.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_cost = new QLabel("Cost:");
		text_cost.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_cost.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_gst = new QLabel("G.S.T 6%:");
		text_gst.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_gst.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_total = new QLabel("Total:");
		text_total.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_total.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		info_layout.addWidget(text_order,0,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(edit_order,0,1);
		info_layout.addWidget(text_name,1,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(edit_name,1,1);
		info_layout.addWidget(text_number,2,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(edit_number,2,1);
		info_layout.addWidget(text_deliver_by,3,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(combo_deliver_by,3,1);
		info_layout.addWidget(text_area,4,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(edit_area,4,1);
		info_layout.addWidget(text_street,5,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(edit_street,5,1);
		info_layout.addWidget(text_pobox,6,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(edit_pobox,6,1);
		info_layout.addWidget(text_note,7,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(textedit_note,7,1,5,1);
		info_layout.addWidget(text_time,12,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(dynamic_time,12,1);
		info_layout.addWidget(text_cost,13,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(dynamic_cost,13,1);
		info_layout.addWidget(text_gst,14,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(dynamic_gst,14,1);
		info_layout.addWidget(text_total,15,0,Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		info_layout.addWidget(dynamic_total,15,1);
		
		//-------------------NEW LAYOUT------------------
		QHBoxLayout action_layout = new QHBoxLayout();
		action_layout.setContentsMargins(5,5,5,5);
		action_layout.setSpacing(2);
		
		fields_layout.addLayout(action_layout);
		
		QPushButton action_submit = new QPushButton("Submit");
		action_submit.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_submit.released.connect(this,"submit()");
		
		action_layout.addWidget(action_submit, 1, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		
		//-------------------SET SETTING------------------
		set_data();
	}
	/**
	 * Submit the information to database
	 */
	private void submit()
    {
		String status="";
//		menu_list;
//		selection_list;
		
		String meal = "";
		String meal_price = "";
		String drink ="";
		String drink_price = "";
		for( int count = 0; count < selection_list.topLevelItemCount(); ++count )
		{
			QTreeWidgetItem item = selection_list.topLevelItem(count);
			if (item.text(3).equals("Meal"))
			{
				meal = meal + "," + item.text(0);
				meal_price = meal_price + "," + item.text(1);
				
			}
			else if (item.text(3).equals("Drink"))
			{
				drink = drink + "," + item.text(0);
				drink_price = drink_price + "," + item.text(1);
			}
		}
		
		// Getting Current Time
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simple_format= new SimpleDateFormat("HH:mm:ss");
        String time_in = simple_format.format(calendar.getTime());
        
        // Complete Time
        String time_out = null;
        
        // Submit Time
        String date = QDate.currentDate().year()+"/"+QDate.currentDate().month()+"/"+QDate.currentDate().day();
        
        // Checking the type of delivery
		if (combo_deliver_by.currentText().equals("Takeaway"))
		{
			status = "Done";
			time_out = time_in;
		}
		else if (combo_deliver_by.currentText().equals("Motorbike"))
		{
			status = "In Progress";
		}
		
		// Submit to Database
		SQLite_Manager.add_order(
				edit_order.text(), 
				edit_name.text(), 
				edit_number.text(), 
				combo_deliver_by.currentText(), 
				edit_area.text(), 
				edit_street.text(), 
				edit_pobox.text(), 
				textedit_note.toPlainText(), 
				time_in, 
				time_out, 
				date, 
				dynamic_total.text(), 
				status, 
				meal, 
				meal_price, 
				drink, 
				drink_price);

	}
	/**
	 * set default setting for widgets 
	 * every time Order Page is opened
	 */
	private void set_data()
	{
		update_menu();
	}
	/**
	 * updating the list by selecting from menu ComboBox
	 */
	private void update_menu()
	{
		menu_list.clear();
		String menu_name = menu_items.currentText();
		if (menu_name.matches("Meal"))
		{
			menu_list.clear();
	    	List<QTreeWidgetItem> ListItems = SQLite_Manager.get_menu("Meal");	
	    	menu_list.addTopLevelItems(ListItems);
		}
		else if (menu_name.matches("Drink"))
		{
			menu_list.clear();
	    	List<QTreeWidgetItem> ListItems = SQLite_Manager.get_menu("Drink");	
	    	menu_list.addTopLevelItems(ListItems);
		}
	}
	/**
	 * add the item from menu list to selected items list.
	 */
	private void add_selected()
	{
		MatchFlags flags = new MatchFlags();
		flags.set(Qt.MatchFlag.MatchExactly);
		List<QTreeWidgetItem> result = selection_list.findItems(menu_list.currentItem().text(0), flags, 0);
		
		QTreeWidgetItem item = new QTreeWidgetItem();
		item.setText(0, menu_list.currentItem().text(0));
		item.setText(1, menu_list.currentItem().text(1));
		item.setText(2, menu_list.currentItem().text(2));
		item.setText(3, combo_deliver_by.currentText());
		selection_list.addTopLevelItem(item);
		filter_listview();
		
	}
	/**
	 * Remove the item from the list of selected items.
	 */
	private void remove_selected()
	{
		List <QTreeWidgetItem> selected_list;
		selected_list = selection_list.selectedItems();
		for (int count = 0; count < selected_list.size(); count++)
		{
			int selected_index;
			selected_index = selection_list.indexOfTopLevelItem(selected_list.get(count));
			selection_list.takeTopLevelItem(selected_index);
			
		}
		filter_listview();
	}
	/**
	 * update the Cost, GST, Total and Time taking 
	 * from list of selected items
	 * 
	 */
	private void filter_listview()
	{
		int total_min = 0;
		double total_cost = 0.0, total_gst = 0.0;
		for( int count = 0; count < selection_list.topLevelItemCount(); ++count )
		{
			QTreeWidgetItem item = selection_list.topLevelItem(count);
			int min = Integer.parseInt(item.text(1).replace(" min",""));
			if (total_min < min){total_min = min;}
			total_cost += Double.parseDouble(item.text(2).replace("RM ",""));
		}
		dynamic_time.setText(Integer.toString(total_min)+" min");
		dynamic_cost.setText("RM " + Double.toString(total_cost));
		if (total_cost > 0.0)
		{
			total_gst = (6/100.0)*total_cost;
			dynamic_gst.setText("RM " + Double.toString(total_gst));
			dynamic_total.setText("RM " + Double.toString(total_gst+total_cost));
		}
		else
		{
			dynamic_gst.setText("RM 0");
			dynamic_total.setText("RM 0");
		}
	}
}