package layout;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

import engine.SQLite_Manager;
import engine.Text_Manager;
import engine.XML_Manager;
import config.App_Setting;
import config.Database_Settings;

/**
 * This is the Resturant Application
 * for cashier to reserve a new table
 * and delivery as well as release bill, 
 * 
 * @author Bernard Rouhi
 *
 */
public class Chalet_Count extends QWidget {
	
	QTreeWidget reservation_view = new QTreeWidget();
	QTreeWidget order_view = new QTreeWidget();
	Reservation_Page reserv_ui = new Reservation_Page();
	Order_Page order_ui = new Order_Page();
	Connection connection = SQLite_Manager.database_connection("reservation");
    
    public Chalet_Count() {

        setWindowTitle(String.format("Chalet Count v%1.2f",1.0));
        setMinimumHeight(600);
        setMinimumWidth(800);
        setGeometry(250, 250, 350, 100);
        setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QWidget"));

        initUI();

        show();
    }
    
    private void initUI() {
		
		QHBoxLayout main_layout = new QHBoxLayout(this);
		main_layout.setContentsMargins(30,50,30,30);
		//-------------------NEW TAB------------------
		QTabWidget tab_widget = new QTabWidget();
		tab_widget.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTabWidget"));
		tab_widget.setTabPosition(QTabWidget.TabPosition.North);
		tab_widget.setLayout(new QVBoxLayout());
		tab_widget.layout().setContentsMargins(5, 5, 5, 5);
		tab_widget.setSizePolicy(QSizePolicy.Policy.MinimumExpanding,QSizePolicy.Policy.MinimumExpanding);
		
		main_layout.addWidget(tab_widget);
		//-------------------NEW WIDGET------------------
		QWidget reservation_widget = new QWidget();
		QVBoxLayout reservation_main_layout = new QVBoxLayout();
		reservation_main_layout.setContentsMargins(0,0,0,0);
		reservation_main_layout.setSpacing(2);
		reservation_widget.setLayout(reservation_main_layout);
		reservation_widget.setSizePolicy(QSizePolicy.Policy.MinimumExpanding,QSizePolicy.Policy.MinimumExpanding);
		
		//-------------------NEW LAYOUT------------------
		QVBoxLayout reservation_layout = new QVBoxLayout();
		reservation_layout.setContentsMargins(0,0,0,0);
		reservation_layout.setSpacing(2);
		
		reservation_view.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		QTreeWidgetItem reservation_header = new QTreeWidgetItem();
		
		reservation_header.setText(0, "Table Number");
		reservation_header.setText(1, "Name");
		reservation_header.setText(2, "Contact Number");
		reservation_header.setText(3, "Person");
		reservation_header.setText(4, "Time-In");
		reservation_header.setText(5, "Date");
		reservation_header.setText(6, "Total");
		
		reservation_view.setHeaderItem(reservation_header);
		
		reservation_layout.addWidget(reservation_view);
		//-------------------NEW LAYOUT------------------
		QHBoxLayout buttons_layout = new QHBoxLayout();
		buttons_layout.setContentsMargins(5,5,5,5);
		buttons_layout.setSpacing(2);
		
		reservation_layout.addLayout(buttons_layout);
		
		QPushButton refresh_action = new QPushButton("Refresh");
		refresh_action.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		refresh_action.released.connect(this,"refresh_reservation_view()");
		QPushButton checkin_action = new QPushButton("Check-in");
		checkin_action.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		checkin_action.released.connect(this,"reservation_ui()");
		QPushButton checkout_action = new QPushButton("Check-out");
		checkout_action.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		checkout_action.released.connect(this,"bill_ui()");
		
		buttons_layout.addWidget(refresh_action);
		buttons_layout.addWidget(checkin_action);
		buttons_layout.addWidget(checkout_action, 1, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignLeft);
		
		reservation_main_layout.addLayout(reservation_layout);
		tab_widget.layout().addWidget(reservation_widget);
		tab_widget.addTab(reservation_widget, "Reservation");
		
		//-------------------NEW WIDGET------------------
		QWidget delivery_widget = new QWidget();
		QVBoxLayout delivery_main_layout = new QVBoxLayout();
		delivery_main_layout.setContentsMargins(0,0,0,0);
		delivery_main_layout.setSpacing(2);
		delivery_widget.setLayout(delivery_main_layout);
		delivery_widget.setSizePolicy(QSizePolicy.Policy.MinimumExpanding,QSizePolicy.Policy.MinimumExpanding);
		
		//-------------------NEW LAYOUT------------------
		QVBoxLayout delivery_layout = new QVBoxLayout();
		delivery_layout.setContentsMargins(0,0,0,0);
		delivery_layout.setSpacing(2);
		
		order_view.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		QTreeWidgetItem order_header = new QTreeWidgetItem();
		
		order_header.setText(0, "Status");
		order_header.setText(1, "Order Number");
		order_header.setText(2, "Customer Name");
		order_header.setText(3, "Customer Number");
		order_header.setText(4, "Deliver Type");
		order_header.setText(5, "Time-In");
		order_header.setText(6, "Time-Out");
		order_header.setText(7, "Total");
		order_header.setText(8, "Area");
		order_header.setText(9, "Street");
		order_header.setText(10, "Postal Code");
		order_header.setText(11, "Note");
		order_header.setText(12, "Date");
		
		order_view.setHeaderItem(order_header);
		
		delivery_layout.addWidget(order_view);
		//-------------------NEW LAYOUT------------------
		QHBoxLayout order_buttons_layout = new QHBoxLayout();
		order_buttons_layout.setContentsMargins(5,5,5,5);
		order_buttons_layout.setSpacing(2);
		
		delivery_layout.addLayout(order_buttons_layout);
		
		QPushButton refresh_order_action = new QPushButton("Refresh");
		refresh_order_action.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		refresh_order_action.released.connect(this,"refresh_order_view()");
		QPushButton checkin_order_action = new QPushButton("Add-Order");
		checkin_order_action.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		checkin_order_action.released.connect(this,"order_ui()");
		QPushButton checkout_order_action = new QPushButton("Print Bill");
		checkout_order_action.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		checkout_order_action.released.connect(this,"order_bill_ui()");
		
		order_buttons_layout.addWidget(refresh_order_action);
		order_buttons_layout.addWidget(checkin_order_action);
		order_buttons_layout.addWidget(checkout_order_action, 1, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignLeft);
		
		delivery_main_layout.addLayout(delivery_layout);
		tab_widget.layout().addWidget(delivery_widget);
		tab_widget.addTab(delivery_widget, "Delivery");
		
		//-------------------INITIAL SETS------------------
		refresh_reservation_view();
		refresh_order_view();
    }
    
    /**
     * refresh reserve list by clearing the list and 
     * getting latest data from reservation database
     */
    private void refresh_reservation_view()
    {
    	reservation_view.clear();
    	List<QTreeWidgetItem> ListItems = SQLite_Manager.read_all("Active");	
    	reservation_view.addTopLevelItems(ListItems);
//    	System.out.println("Refreshing");
//    	String today_database = "reservation_" + QDate.currentDate().year() + QDate.currentDate().month() + QDate.currentDate().day();
//    	NodeList nodes = XML_Manager.read_xml(today_database, Database_Settings.Reservation_Group);
//    	
//    	for (int count = 0;count<nodes.getLength();count++)
//    	{
//    		Node node =nodes.item(count);
//    		if (node.getNodeType() == Node.ELEMENT_NODE)
//    		{
//    			// {"Table_Number", "Name", "Contact_Number", "Person", "Time-in", "Date", "Total"}
//    			Element element = (Element) node;
//    			System.out.println(element.getAttribute("ID"));
//    			System.out.println(element.getElementsByTagName("Table_Number").item(0).getTextContent());
//    			System.out.println(element.getElementsByTagName("Name").item(0).getTextContent());
//    			System.out.println(element.getElementsByTagName("Contact_Number").item(0).getTextContent());
//    			System.out.println(element.getElementsByTagName("Person").item(0).getTextContent());
//    			System.out.println(element.getElementsByTagName("Time-in").item(0).getTextContent());
//    			System.out.println(element.getElementsByTagName("Date").item(0).getTextContent());
//    			System.out.println(element.getElementsByTagName("Total").item(0).getTextContent());
//    		}
//    		
//    	}
	}
    
    private void reservation_ui()
    {
		reserv_ui.show();
	}
    
    private void bill_ui()
    {
    	if (reservation_view.currentItem() != null)
    	{
	    	QTreeWidgetItem current_item = reservation_view.currentItem();
	    	Bill_Page bill_ui = new Bill_Page(current_item);
			bill_ui.show();
    	}
	}
    
    /**
     * refresh order list by clearing the list and 
     * getting latest data from order database
     */
    private void refresh_order_view()
    {
    	order_view.clear();
    	List<QTreeWidgetItem> ListItems = SQLite_Manager.query_order_database();	
    	order_view.addTopLevelItems(ListItems);
	}
    
    private void order_ui()
    {
    	order_ui.show();
	}
    
    private void order_bill_ui()
    {
    	if (order_view.currentItem() != null)
    	{
	    	QTreeWidgetItem current_item = order_view.currentItem();
	    	Delivery_Bill_Page delivery_ui = new Delivery_Bill_Page(current_item);
	    	delivery_ui.show();
    	}
	}

    public static void main(String[] args) {
        QApplication.initialize(args);
        new Chalet_Count();
        QApplication.execStatic();
        QApplication.shutdown();
    }
}

