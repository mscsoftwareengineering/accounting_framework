package layout;

import com.trolltech.qt.core.Qt;
import com.trolltech.qt.gui.*;

import config.App_Setting;
import engine.Text_Manager;

/**
 * This is Delivery Bill page to release bill
 * for order
 * 
 * @author Bernard Rouhi
 *
 */
public class Delivery_Bill_Page extends QWidget
{
	QTreeWidgetItem reservation_header = new QTreeWidgetItem();
	QTreeWidgetItem current_item;
	QLabel dynamic_orderNum = new QLabel("Enter");
	QLabel dynamic_name = new QLabel("Enter");
	QLabel dynamic_number = new QLabel("Enter");
	QLabel dynamic_deliver_by = new QLabel("Enter");
	QLabel dynamic_timeout = new QLabel("Enter");
	QTreeWidget list_dish = new QTreeWidget();
	QTreeWidget list_drink = new QTreeWidget();
	QLabel dynamic_address = new QLabel("Enter");
	QLabel dynamic_area = new QLabel("Enter");
	QLabel dynamic_pobox = new QLabel("Enter");
	QLabel dynamic_cost = new QLabel("Enter");
	QLabel dynamic_gst = new QLabel("Enter");
	QLabel dynamic_total = new QLabel("Enter");
	
	public Delivery_Bill_Page(QTreeWidgetItem item)
	{
		current_item = item;
		setWindowTitle(String.format("Delivery Bill v%1.2f",1.0));
        setMinimumHeight(600);
        setMinimumWidth(400);
        setGeometry(250, 250, 350, 100);
        setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QWidget"));
        
        initUI();

	}
	private void initUI() 
	{
		QVBoxLayout main_layout = new QVBoxLayout(this);
		main_layout.setContentsMargins(0,0,0,0);
		main_layout.setSpacing(0);
		
		//-------------------NEW LAYOUT------------------
		QGridLayout information_layout = new QGridLayout();
		information_layout.setContentsMargins(5,5,5,5);
		information_layout.setSpacing(2);
		
		main_layout.addLayout(information_layout);
		
		QLabel text_orderNum = new QLabel("Order Number:");
		text_orderNum.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_orderNum.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_name = new QLabel("Name:");
		text_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_number = new QLabel("Number:");
		text_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_deliver_by = new QLabel("Deliver By:");
		text_deliver_by.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_deliver_by.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_timeout = new QLabel("Time-out:");
		text_timeout.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_timeout.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_dish = new QLabel("Dish:");
		text_dish.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		list_dish.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		
		reservation_header.setText(0, "Name");
		reservation_header.setText(1, "Number");
		reservation_header.setText(2, "Total");
		
		list_dish.setHeaderItem(reservation_header);
		
		QLabel text_drink = new QLabel("Drink:");
		text_drink.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		list_drink.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		
		list_drink.setHeaderItem(reservation_header);
		
		QLabel text_address = new QLabel("Address:");
		text_address.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_address.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_area = new QLabel("Area:");
		text_area.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_area.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_pobox = new QLabel("Postal Card:");
		text_pobox.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_pobox.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_cost = new QLabel("Cost:");
		text_cost.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_cost.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_gst = new QLabel("G.S.T 6%:");
		text_gst.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_gst.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_total = new QLabel("Total:");
		text_total.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_total.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		information_layout.addWidget(text_orderNum,0,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_orderNum,0,1);
		
		information_layout.addWidget(text_name,1,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_name,1,1);
		
		information_layout.addWidget(text_number,2,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_number,2,1);
		
		information_layout.addWidget(text_deliver_by,3,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_deliver_by,3,1);
		
		information_layout.addWidget(text_timeout,4,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_timeout,4,1);
		
		information_layout.addWidget(text_dish,5,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(list_dish,5,1,6,5);
		
		information_layout.addWidget(text_drink,11,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(list_drink,11,1,6,5);
		
		information_layout.addWidget(text_address,17,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_address,17,1);
		
		information_layout.addWidget(text_area,18,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_area,18,1);
		
		information_layout.addWidget(text_pobox,19,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_pobox,19,1);
		
		information_layout.addWidget(text_cost,20,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_cost,20,1);
		
		information_layout.addWidget(text_gst,21,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_gst,21,1);
		
		information_layout.addWidget(text_total,22,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_total,22,1);
		
		//-------------------NEW LAYOUT------------------
		QHBoxLayout action_layout = new QHBoxLayout();
		action_layout.setContentsMargins(5,5,5,5);
		action_layout.setSpacing(2);
		
		main_layout.addLayout(action_layout);
		
		QPushButton action_cancel = new QPushButton("Cancel");
		action_cancel.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_cancel.released.connect(this,"cancel_window()");
		
		QPushButton action_close = new QPushButton("Close");
		action_close.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_close.released.connect(this,"close_window()");
		
		QPushButton action_checkin = new QPushButton("Checkin");
		action_checkin.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_checkin.released.connect(this,"checkin_window()");
		
		action_layout.addWidget(action_cancel);
		action_layout.addWidget(action_close);
		action_layout.addWidget(action_checkin,1, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignLeft);
		//-------------------SET SETTING------------------
		set_data();
		
	}
	/**
	 * Use to close the Delivery Page
	 */
	private void close_window()
    {
		close();
	}
	/**
	 * use to cancel the current order,
	 * it will effect the database 
	 */
	private void cancel_window()
    {
		
	}
	/**
	 * Use to print the order
	 */
	private void checkin_window()
    {

	}
	/**
	 * 0 - Status
	 * 1 - Order Number
	 * 2 - Customer Name
	 * 3 - Customer Number
	 * 4 - Deliver Type
	 * 5 - Time-In
	 * 6 - Time-Out
	 * 7 - Total
	 * 8 - Area
	 * 9 - Street
	 * 10 - Postal Code
	 * 11 - Note
	 * 12 - Date
	 * 
	 */
	private void set_data()
	{
//		list_dish = new QTreeWidget();
//		list_drink = new QTreeWidget();
		dynamic_orderNum.setText(current_item.text(1));
		dynamic_name.setText(current_item.text(2));
		dynamic_number.setText(current_item.text(3));
		dynamic_deliver_by.setText(current_item.text(4));
		dynamic_timeout.setText(current_item.text(6));
		dynamic_address.setText(current_item.text(9));
		dynamic_area.setText(current_item.text(8));
		dynamic_pobox.setText(current_item.text(10));
//		dynamic_cost.setText(arg__1);
//		dynamic_gst.setText(arg__1);
//		dynamic_total.setText(arg__1);
	}
}