package layout;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.trolltech.qt.core.Qt;
import com.trolltech.qt.gui.*;

import config.App_Setting;
import engine.Core;
import engine.Text_Manager;

/**
 * Bill page is for releasing bill for 
 * given table
 * 
 * @author Bernard Rouhi
 *
 */
public class Bill_Page extends QWidget
{
	QTreeWidgetItem reservation_header = new QTreeWidgetItem();
	QTreeWidgetItem current_item;
	QLabel dynamic_tableNum = new QLabel("Enter");
	QLabel dynamic_name = new QLabel("Enter");
	QLabel dynamic_number = new QLabel("Enter");
	QLabel dynamic_person = new QLabel("Enter");
	QLabel dynamic_timein = new QLabel("Enter");
	QLabel dynamic_timeout = new QLabel("Enter");
	QTreeWidget list_dish = new QTreeWidget();
	QTreeWidget list_drink = new QTreeWidget();
	QLabel dynamic_cost = new QLabel("Enter");
	QLabel dynamic_gst = new QLabel("Enter");
	QLabel dynamic_total = new QLabel("Enter");
	
	public Bill_Page(QTreeWidgetItem item)
	{
		current_item = item;
		setWindowTitle(String.format("Bill v%1.2f",1.0));
        setMinimumHeight(600);
        setMinimumWidth(400);
        setGeometry(250, 250, 350, 100);
        setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QWidget"));
        
        initUI();
        pass_data();

	}
	private void initUI() 
	{
		// Layout setup
		QVBoxLayout main_layout = new QVBoxLayout(this);
		main_layout.setContentsMargins(0,0,0,0);
		main_layout.setSpacing(0);
		
		//-----------------NEW LAYOUT--------------------
		QGridLayout information_layout = new QGridLayout();
		information_layout.setContentsMargins(5,5,5,5);
		information_layout.setSpacing(2);
		
		main_layout.addLayout(information_layout);
		
		QLabel text_tableNum = new QLabel("Table Number:");
		text_tableNum.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_tableNum.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_name = new QLabel("Name:");
		text_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_name.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_number = new QLabel("Number:");
		text_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_number.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_person = new QLabel("Person:");
		text_person.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_person.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_timein = new QLabel("Time-in:");
		text_timein.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_timein.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_timeout = new QLabel("Time-out:");
		text_timeout.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_timeout.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_dish = new QLabel("Dish:");
		text_dish.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		list_dish.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		
		reservation_header.setText(0, "Name");
		reservation_header.setText(1, "Number");
		reservation_header.setText(2, "Total");
		
		list_dish.setHeaderItem(reservation_header);
		
		QLabel text_drink = new QLabel("Drink:");
		text_drink.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		list_drink.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QTreeWidget"));
		
		list_drink.setHeaderItem(reservation_header);
		
		QLabel text_cost = new QLabel("Cost:");
		text_cost.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_cost.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_gst = new QLabel("G.S.T 6%:");
		text_gst.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_gst.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		QLabel text_total = new QLabel("Total:");
		text_total.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		dynamic_total.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"QLabel"));
		
		information_layout.addWidget(text_tableNum,0,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_tableNum,0,1);
		
		information_layout.addWidget(text_name,1,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_name,1,1);
		
		information_layout.addWidget(text_number,2,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_number,2,1);
		
		information_layout.addWidget(text_person,3,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_person,3,1);
		
		information_layout.addWidget(text_timein,4,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_timein,4,1);
		
		information_layout.addWidget(text_timeout,5,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_timeout,5,1);
		
		information_layout.addWidget(text_dish,6,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(list_dish,6,1,6,5);
		
		information_layout.addWidget(text_drink,13,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(list_drink,13,1,6,5);
		
		information_layout.addWidget(text_cost,19,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_cost,19,1);
		
		information_layout.addWidget(text_gst,20,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_gst,20,1);
		
		information_layout.addWidget(text_total,21,0, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignRight);
		information_layout.addWidget(dynamic_total,21,1);
		
		//-----------------NEW LAYOUT--------------------
		QHBoxLayout action_layout = new QHBoxLayout();
		action_layout.setContentsMargins(5,5,5,5);
		action_layout.setSpacing(2);
		
		main_layout.addLayout(action_layout);
		
		QPushButton action_cancel = new QPushButton("Cancel");
		action_cancel.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_cancel.released.connect(this,"cancel_window()");
		
		QPushButton action_close = new QPushButton("Close");
		action_close.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_close.released.connect(this,"close_window()");
		
		QPushButton action_checkin = new QPushButton("Checkin");
		action_checkin.setStyleSheet(Text_Manager.read_stylesheet(App_Setting.CURRENT_UI,"Action_Button"));
		action_checkin.released.connect(this,"checkin_window()");
		
		action_layout.addWidget(action_cancel);
		action_layout.addWidget(action_close);
		action_layout.addWidget(action_checkin,1, Qt.AlignmentFlag.AlignCenter,Qt.AlignmentFlag.AlignLeft);
		
		//-------------------SET SETTING------------------
		set_data();
		
	}

	private void pass_data() {
		
	}
	/**
	 * Use to close the Delivery Page
	 */
	private void close_window()
    {
		close();
	}
	/**
	 * use to cancel the current order,
	 * it will effect the database 
	 */
	private void cancel_window()
    {
	
	}
	/**
	 * Use to print the order
	 */
	private void checkin_window()
    {

	}
	
	/**
	 * Set the value base on give QTreeWidgetItem
	 * 
	 * Columns of the item:
	 * 0 - "Table Name"
	 * 1 - "Customer name" 
	 * 2 - "Customer Number"
	 * 3 - "Size"
	 * 4 - "Time-in"
	 * 5 - "Date"
	 * 6 - "Total"
	 * 7 - "ID"
	 * 
	 * @author Bernard Rouhi
	 */
	private void set_data()
	{
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simple_format= new SimpleDateFormat("HH:mm:ss");
		
		dynamic_tableNum.setText(current_item.text(0));
		dynamic_name.setText(current_item.text(1));
		dynamic_number.setText(current_item.text(2));
		dynamic_person.setText(current_item.text(3));
		dynamic_timein.setText(current_item.text(4));
		dynamic_timeout.setText(simple_format.format(calendar.getTime()));;
//		list_dish = new QTreeWidget();
//		list_drink = new QTreeWidget();
		int[] price_list ={Integer.parseInt(current_item.text(6))}; 
		double[] prices = Core.cost(price_list, 6);
		dynamic_cost.setText("RM " + prices[0]);
		dynamic_gst.setText("RM " + prices[1]);
		dynamic_total.setText("RM " + prices[2]);
	}
}