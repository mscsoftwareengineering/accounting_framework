package config;

public class Database_Settings {
	
	public static final String Reservation_Group = "Reserved";
	public static final String[] Reservation_Titles = {"Table_Number", "Name", "Contact_Number", "Person", "Time-in", "Date", "Total"};
	
	public static final String Order_Group = "Ordered";
	public static final String[] Order_Titles = {"Order Number", "Name", "Contact Number", "Deliver By", "Order Time", "Time Out", "Time In", "Total"};
}
