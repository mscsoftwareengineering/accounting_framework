package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

/**
 * Managing Food Order Database 
 * 
 * @author MohamadRaziman
 *
 */
public class DBFoodOrder {
	
	String conString = "jdbc:mysql://localhost:3306/restaurant";
	String username="root";
	String password="";


	/**
	 * Add new Row to the foodorder Table
	 * 
	 * @param tableNum Table Number
	 * @param nasiLemak NasiLemak Meal
	 * @param chickenChop Chicken Chop Meal
	 * @param friedNoodles Fired Noodles Meal
	 * @param mandiChicken Mandi Chicken Meal
	 * @param spaghetti Spaghetti Meal
	 * @param totalPrice Total Price of Order
	 * @param totalPriceGST 6% GST of Total Price
	 * @param foodStatus Status
	 * @param payStatus Payment Status
	 * @return True if Success and False if fail
	 */
	public Boolean add(String tableNum, String nasiLemak, String chickenChop, 
			String friedNoodles, String mandiChicken, String spaghetti, String totalPrice, String totalPriceGST, String foodStatus, String payStatus)
	{
		//SQL Statement
		String sql="INSERT INTO restaurant.foodorder VALUES (default, '"+tableNum+"','"+nasiLemak+"','"+chickenChop+"','"+friedNoodles+"','"+mandiChicken+"','"+spaghetti+"','"+totalPrice+"','"+totalPriceGST+"','"+foodStatus+"','"+payStatus+"')";
		
		try 
		{
			//connection
			Connection con=DriverManager.getConnection(conString, username, password);
			
			//prepare statement
			Statement s=con.prepareStatement(sql);
			
			s.execute(sql);
			
			return true;
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Query of all date from foodorder Table
	 * @return DefaultTableModel of Table Data
	 */
    public DefaultTableModel getData() {
        //ADD COLUMNS TO TABLE MODEL
        DefaultTableModel dm = new DefaultTableModel();
        dm.addColumn("ID");
        dm.addColumn("Table No");
        dm.addColumn("Nasi Lemak");
        dm.addColumn("Chicken Chop");
        dm.addColumn("Fried Noodles");
        dm.addColumn("Mandi Chicken");
        dm.addColumn("Spaghetti");
        dm.addColumn("Price");
        dm.addColumn("Price(GST)");
        dm.addColumn("Status");
        dm.addColumn("Payment");
        //SQL STATEMENT
        String sql = "SELECT * FROM foodorder";
        try {
            Connection con = DriverManager.getConnection(conString, username, password);
            //PREPARED STMT
            Statement s = con.prepareStatement(sql);
            ResultSet rs = s.executeQuery(sql);
            //LOOP THRU GETTING ALL VALUES
            while (rs.next()) {
                //GET VALUES
                String id = rs.getString(1);
                String tableNum = rs.getString(2);
                String nasiLemak = rs.getString(3);
                String chickenChop = rs.getString(4);
                String friedNoodles = rs.getString(5);
                String mandiChicken = rs.getString(6);
                String spaghetti = rs.getString(7);
                String totalPrice = rs.getString(8);
                String totalPriceGST = rs.getString(9);
                String foodStatus = rs.getString(10);
                String payStatus = rs.getString(11);
                
                dm.addRow(new String[]{id, tableNum, nasiLemak, chickenChop, friedNoodles, mandiChicken, spaghetti, totalPrice, 
                		totalPriceGST, foodStatus, payStatus});
            }
            return dm;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    /**
     * Updating the foodorder Table by giving ID number
     * @param id Table ID Number
     * @param btnCompleteOrder setting foodStatus to Order complete
     * @return True if Success and False if fail
     */
    public Boolean updateOrder(String id, String btnCompleteOrder) {
        //SQL STMT
        //String sql = "UPDATE foodorder SET foodStatus = 'Completed' WHERE reserve_id='" + id + "'";
        String sql = "UPDATE foodorder SET foodStatus = '"+ btnCompleteOrder +"' where id = '" + id + "' ";
        try {
            //GET COONECTION
            Connection con = DriverManager.getConnection(conString, username, password);
            //STATEMENT
            Statement s = con.prepareStatement(sql);
            //EXECUTE
            s.execute(sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    /**
     * Updating the foodorder Table by giving ID number
     * @param id Table ID Number
     * @param btnCookOrder setting foodStatus to preparing Dish
     * @return True if Success and False if fail
     */
    public Boolean cookOrder(String id, String btnCookOrder) {
        //SQL STMT
        //String sql = "UPDATE foodorder SET foodStatus = 'Completed' WHERE reserve_id='" + id + "'";
        String sql = "UPDATE foodorder SET foodStatus = '"+ btnCookOrder +"' where id = '" + id + "' ";
        try {
            //GET COONECTION
            Connection con = DriverManager.getConnection(conString, username, password);
            //STATEMENT
            Statement s = con.prepareStatement(sql);
            //EXECUTE
            s.execute(sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    /**
     * Updating the foodorder Table by giving ID number
     * @param id Table ID Number
     * @param btnPaid setting payStatus to Order Paid
     * @return True if Success and False if fail
     */
    public Boolean orderPaid(String id, String btnPaid) {
        //SQL STMT
        //String sql = "UPDATE foodorder SET foodStatus = 'Completed' WHERE reserve_id='" + id + "'";
        String sql = "UPDATE foodorder SET payStatus = '"+ btnPaid +"' where id = '" + id + "' ";
        try {
            //GET COONECTION
            Connection con = DriverManager.getConnection(conString, username, password);
            //STATEMENT
            Statement s = con.prepareStatement(sql);
            //EXECUTE
            s.execute(sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    /**
     * Deleting a row from foodorder table by giving ID number
     * @param id Row ID
     * @return True if Success and False if fail
     */
    public Boolean delete(String id)
    {
        //SQL STMT
        String sql="DELETE FROM foodorder WHERE id ='"+id+"'";
        
        
        try
        {
            //GET COONECTION
            Connection con=DriverManager.getConnection(conString, username, password);
            
            //STATEMENT
            Statement s=con.prepareStatement(sql);
            
            //EXECUTE
            s.execute(sql);
            
            return true;
            
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }
}