package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

/**
 * Managing Storage Database 
 * 
 * @author MohamadRaziman
 *
 */
public class DBStorage {
	
	String conString = "jdbc:mysql://localhost:3306/restaurant";
	String username="root";
	String password="";

	/**
	 *  Add new Row to the storage
	 * 
	 * @param store_item Item Name
	 * @param store_price Item Price
	 * @param store_quantity Item Quantity
	 * @param store_budget Item Budget
	 * @param store_total Item Total
	 * @param store_month Item Purchased Month
	 * @return True if Success and False if fail
	 */
	public Boolean add(String store_item, String store_price, String store_quantity, String store_budget, String store_total, String store_month)
	{
		//SQL Statement
		String sql="INSERT INTO restaurant.storage VALUES (default, '"+store_item+"','"+store_price+"','"+store_quantity+"','"+store_budget+"','"+store_total+"','"+store_month+"')";
		
		try 
		{
			//connection
			Connection con=DriverManager.getConnection(conString, username, password);
			
			//prepare statement
			Statement s=con.prepareStatement(sql);
			
			s.execute(sql);
			
			return true;
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Query of all date from storage Table
	 * @return DefaultTableModel of Table Data
	 */
    public DefaultTableModel getData() {
        //ADD COLUMNS TO TABLE MODEL
        DefaultTableModel dm = new DefaultTableModel();
        dm.addColumn("ID");
        dm.addColumn("Item");
        dm.addColumn("Price (RM)");
        dm.addColumn("Quantity");
        dm.addColumn("Budget Left");
        dm.addColumn("Total (RM)");
        dm.addColumn("Month");
        //SQL STATEMENT
        String sql = "SELECT * FROM storage";
        try {
            Connection con = DriverManager.getConnection(conString, username, password);
            //PREPARED STMT
            Statement s = con.prepareStatement(sql);
            ResultSet rs = s.executeQuery(sql);
            //LOOP THRU GETTING ALL VALUES
            while (rs.next()) {
                //GET VALUES
                String id = rs.getString(1);
                String store_item = rs.getString(2);
                String store_price = rs.getString(3);
                String store_quantity = rs.getString(4);
                String store_budget = rs.getString(5);
                String store_total = rs.getString(6);
                String store_month = rs.getString(7);
                dm.addRow(new String[]{id, store_item, store_price, store_quantity, store_budget, store_total, store_month});
            }
            return dm;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
 
    /**
     * Deleting a row from storage table by giving ID number
     * @param id Row ID Number
     * @return True if Success and False if fail
     */
    public Boolean delete(String id)
    {
        //SQL STMT
        String sql="DELETE FROM storage WHERE id ='"+id+"'";
        
        
        try
        {
            //GET COONECTION
            Connection con=DriverManager.getConnection(conString, username, password);
            
            //STATEMENT
            Statement s=con.prepareStatement(sql);
            
            //EXECUTE
            s.execute(sql);
            
            return true;
            
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }
}