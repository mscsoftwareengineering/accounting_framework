package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

/**
 * Managing Food Order Database 
 * 
 * @author MohamadRaziman
 *
 */
public class DBTableReserve {
	
	String conString = "jdbc:mysql://localhost:3306/restaurant";
	String username="root";
	String password="";

	/**
	 * Add new Row to the reserve Table
	 * 
	 * @param cust_name Customer Name
	 * @param cust_phone Customer Number
	 * @param table_num Table Number
	 * @param time Reserved Time
	 * @param no_person Table Size
	 * @return True if Success and False if fail
	 */
	public Boolean add(String cust_name, String cust_phone, String table_num, String time, String no_person)
	{
		//SQL Statement
		String sql="INSERT INTO restaurant.reserve VALUES (default, '"+cust_name+"','"+cust_phone+"','"+table_num+"','"+time+"','"+no_person+"')";
		
		try 
		{
			//connection
			Connection con=DriverManager.getConnection(conString, username, password);
			
			//prepare statement
			Statement s=con.prepareStatement(sql);
			
			s.execute(sql);
			
			return true;
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Query of all date from reserve Table
	 * @return DefaultTableModel of Table Data
	 */
    public DefaultTableModel getData() {
        //ADD COLUMNS TO TABLE MODEL
        DefaultTableModel dm = new DefaultTableModel();
        dm.addColumn("ID");
        dm.addColumn("Customer Name");
        dm.addColumn("Customer Phone");
        dm.addColumn("Table Number");
        dm.addColumn("Time");
        dm.addColumn("No. of Person");
        //SQL STATEMENT
        String sql = "SELECT * FROM reserve";
        try {
            Connection con = DriverManager.getConnection(conString, username, password);
            //PREPARED STMT
            Statement s = con.prepareStatement(sql);
            ResultSet rs = s.executeQuery(sql);
            //LOOP THRU GETTING ALL VALUES
            while (rs.next()) {
                //GET VALUES
                String id = rs.getString(1);
                String cust_name = rs.getString(2);
                String cust_phone = rs.getString(3);
                String table_num = rs.getString(4);
                String time = rs.getString(5);
                String no_person = rs.getString(6);
                dm.addRow(new String[]{id, cust_name, cust_phone, table_num, time, no_person});
            }
            return dm;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Updating the reserve Table by giving ID number
     * 
     * @param id ID of Row
     * @param cust_name Customer Name
     * @param cust_phone Customer Number
     * @param table_num Table Number
     * @param time Time of Reservation
     * @param no_person Size of table
     * @return True if Success and False if fail
     */
    public Boolean update(String id, String cust_name, String cust_phone, String table_num, String time, String no_person) {
        //SQL STMT
        String sql = "UPDATE reserve SET cust_name ='" + cust_name + "',cust_phone='" + cust_phone + "',table_num='" + table_num + "'"
        		+ ",time='" + time + "',no_person='" + no_person + "' WHERE reserve_id='" + id + "'";
        try {
            //GET COONECTION
            Connection con = DriverManager.getConnection(conString, username, password);
            //STATEMENT
            Statement s = con.prepareStatement(sql);
            //EXECUTE
            s.execute(sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    /**
     * Deleting a row from reserve table by giving ID number
     * @param id ID of the Row
     * @return True if Success and False if fail
     */
    public Boolean delete(String id)
    {
        //SQL STMT
        String sql="DELETE FROM reserve WHERE reserve_id ='"+id+"'";
        
        
        try
        {
            //GET COONECTION
            Connection con=DriverManager.getConnection(conString, username, password);
            
            //STATEMENT
            Statement s=con.prepareStatement(sql);
            
            //EXECUTE
            s.execute(sql);
            
            return true;
            
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }
}