package system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import main.Option;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Choice;
import javax.swing.JButton;
/**
 * Chalet Run User Interface
 * 
 * @author MohamadRaziman
 *
 */
public class ChaletRun extends JFrame {

	private JPanel contentPane;
	private JTextField txtCustomerName;
	private JTextField txtCustomerPhone;
	private JTextField txtOrderID;
	private JTextField txtNumOrder;
	private JTextField txtTimeResult;
	private JTextField txtCustNameResult;
	private JTextField txtLocationResult;

	/**
	 * Launch the application.
	 * @param args Arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChaletRun frame = new ChaletRun();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChaletRun() {
		setTitle("Chalet Run");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 489, 314);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMainMenu = new JMenu("Main Menu");
		menuBar.add(mnMainMenu);
		
		JMenuItem mntmSelectSystem = new JMenuItem("Select System");
		mntmSelectSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Option back = new Option();
				back.setVisible(true);
			}
		});
		mnMainMenu.add(mntmSelectSystem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblChaletOrderDelivery = new JLabel("Delivery Track and Time Estimation");
		lblChaletOrderDelivery.setBounds(132, 11, 166, 14);
		contentPane.add(lblChaletOrderDelivery);
		
		JLabel lblCustomerName = new JLabel("Customer Name");
		lblCustomerName.setBounds(10, 36, 114, 14);
		contentPane.add(lblCustomerName);
		
		JLabel lblCustomerPhone = new JLabel("Customer Phone");
		lblCustomerPhone.setBounds(10, 71, 92, 14);
		contentPane.add(lblCustomerPhone);
		
		JLabel lblLocation = new JLabel("Location");
		lblLocation.setBounds(10, 171, 75, 14);
		contentPane.add(lblLocation);
		
		JLabel lblorderID = new JLabel("Order ID");
		lblorderID.setBounds(10, 105, 103, 14);
		contentPane.add(lblorderID);
		
		txtCustomerName = new JTextField();
		txtCustomerName.setBounds(139, 36, 86, 20);
		contentPane.add(txtCustomerName);
		txtCustomerName.setColumns(10);
		
		txtCustomerPhone = new JTextField();
		txtCustomerPhone.setBounds(139, 68, 86, 20);
		contentPane.add(txtCustomerPhone);
		txtCustomerPhone.setColumns(10);
		
		txtOrderID = new JTextField();
		txtOrderID.setBounds(139, 102, 86, 20);
		contentPane.add(txtOrderID);
		txtOrderID.setColumns(10);
		
		txtNumOrder = new JTextField();
		txtNumOrder.setColumns(10);
		txtNumOrder.setBounds(139, 133, 86, 20);
		contentPane.add(txtNumOrder);
		
		JLabel lblnumberofOrder = new JLabel("Number of Order");
		lblnumberofOrder.setBounds(10, 136, 103, 14);
		contentPane.add(lblnumberofOrder);
		
		Choice choiceLoc = new Choice();
		choiceLoc.add("Abu Dhabi");
		choiceLoc.add("Ajman");
		choiceLoc.add("Dubai");
		choiceLoc.add("Sharjah");
		choiceLoc.setBounds(141, 171, 84, 20);
		contentPane.add(choiceLoc);
		
		JButton btnNewButton = new JButton("Estimate Time");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {		
	            String data = choiceLoc.getItem(choiceLoc.getSelectedIndex());
	            txtLocationResult.setText(data);
			}
		});
		btnNewButton.setBounds(10, 207, 215, 23);
		contentPane.add(btnNewButton);
		
		JPanel panel = new JPanel();
		panel.setBounds(235, 36, 228, 155);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblTimeEstimated = new JLabel("Time estimated:");
		lblTimeEstimated.setBounds(10, 95, 98, 14);
		panel.add(lblTimeEstimated);
		
		txtTimeResult = new JTextField();
		txtTimeResult.setEditable(false);
		txtTimeResult.setBounds(118, 92, 100, 20);
		panel.add(txtTimeResult);
		txtTimeResult.setColumns(10);
		
		JLabel lblCustomerName_1 = new JLabel("Customer Name:");
		lblCustomerName_1.setBounds(10, 25, 98, 14);
		panel.add(lblCustomerName_1);
		
		txtCustNameResult = new JTextField();
		txtCustNameResult.setEditable(false);
		txtCustNameResult.setColumns(10);
		txtCustNameResult.setBounds(118, 22, 100, 20);
		panel.add(txtCustNameResult);
		
		JLabel lblLocation_1 = new JLabel("Location:");
		lblLocation_1.setBounds(10, 58, 98, 14);
		panel.add(lblLocation_1);
		
		txtLocationResult = new JTextField();
		txtLocationResult.setEditable(false);
		txtLocationResult.setColumns(10);
		txtLocationResult.setBounds(118, 55, 100, 20);
		panel.add(txtLocationResult);
		
		JButton btnDeliver = new JButton("Deliver");
		btnDeliver.setBounds(235, 207, 228, 23);
		contentPane.add(btnDeliver);
	}
}
