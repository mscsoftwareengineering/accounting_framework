/**
* @version 1.1
* @author MohamadRaziman
*/
package system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import calculation.OrderCalculation;
import database.DBFoodOrder;
import database.DBTableReserve;
import main.Option;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.SpringLayout;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
/**
 * Chalet Table User Interface
 * 
 * @author MohamadRaziman
 *
 */
public class ChaletTable extends JFrame {

	private JPanel contentPane;
	private JTextField num_NasiLemak;
	private JTextField num_ChickenChop;
	private JTextField num_FriedNoodles;
	private JTextField num_MandiChicken;
	private JTextField num_Spaghetti;
	private JTextField txtpriceNasiLemak;
	private JTextField txtpriceChickenChop;
	private JTextField txtpriceFriedNoodles;
	private JTextField txtpriceMandiChicken;
	private JTextField txtpriceSpaghetti;
	private JTextField txtpriceTotal;
	private JTextField txtpriceTotalGST;
	private JTextField textTableNum;
	private JTextField txtFoodStatus;
	private JTextField txtPayStatus;
	private JTextField txtNonmember;

	/**
	 * Launch the application.
	 * @param args Arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChaletTable frame = new ChaletTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChaletTable() {
		setTitle("Chalet Food Order");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 423, 636);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Main Menu");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmSelectSystem = new JMenuItem("Select System");
		mntmSelectSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Option back = new Option();
				back.setVisible(true);
			}
		});
		mnNewMenu.add(mntmSelectSystem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 57, 77, 256);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(10, 9, 57, 14);
		panel.add(lblQuantity);
		
		num_NasiLemak = new JTextField();
		num_NasiLemak.setBounds(10, 49, 42, 20);
		panel.add(num_NasiLemak);
		num_NasiLemak.setColumns(10);
		
		num_ChickenChop = new JTextField();
		num_ChickenChop.setColumns(10);
		num_ChickenChop.setBounds(10, 89, 42, 20);
		panel.add(num_ChickenChop);
		
		num_FriedNoodles = new JTextField();
		num_FriedNoodles.setColumns(10);
		num_FriedNoodles.setBounds(10, 130, 42, 20);
		panel.add(num_FriedNoodles);
		
		num_MandiChicken = new JTextField();
		num_MandiChicken.setColumns(10);
		num_MandiChicken.setBounds(10, 171, 42, 20);
		panel.add(num_MandiChicken);
		
		num_Spaghetti = new JTextField();
		num_Spaghetti.setColumns(10);
		num_Spaghetti.setBounds(10, 212, 42, 20);
		panel.add(num_Spaghetti);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(83, 57, 207, 256);
		contentPane.add(panel_1);
		
		JLabel label_1 = new JLabel("Food Menu");
		label_1.setBounds(21, 9, 121, 14);
		panel_1.add(label_1);
		
		JButton btnNasiLemak = new JButton("Nasi Lemak");
		btnNasiLemak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {					
			if (num_NasiLemak.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Nasi lemak quantity is empty", "Error!", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				int quantity = Integer.parseInt(num_NasiLemak.getText());
				double result = 1.50*quantity;
				txtpriceNasiLemak.setText(Double.toString(result));  
			}}
		});
		btnNasiLemak.setBounds(10, 48, 180, 23);
		panel_1.add(btnNasiLemak);
		
		JButton btnChickenChop = new JButton("Chicken Chop");
		btnChickenChop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {					
					if (num_ChickenChop.getText().equals("")){
						JOptionPane.showMessageDialog(null, "Chicken Chop quantity is empty", "Error!", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						int quantity = Integer.parseInt(num_ChickenChop.getText());
						double result = 7.5*quantity;
						txtpriceChickenChop.setText(Double.toString(result));  
					}
				}
		});
		btnChickenChop.setBounds(10, 87, 180, 23);
		panel_1.add(btnChickenChop);
		
		JButton btnFriedNoodles = new JButton("Fried Noodles");
		btnFriedNoodles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {					
				if (num_FriedNoodles.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Fried Noodles quantity is empty", "Error!", JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					int quantity = Integer.parseInt(num_FriedNoodles.getText());
					double result = 3.5*quantity;
					txtpriceFriedNoodles.setText(Double.toString(result));  
				}
			}
		});
		btnFriedNoodles.setBounds(10, 129, 180, 23);
		panel_1.add(btnFriedNoodles);
		
		JButton btnMandiChicken = new JButton("Mandi Chicken");
		btnMandiChicken.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {					
				if (num_MandiChicken.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Mandi Chicken quantity is empty", "Error!", JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					int quantity = Integer.parseInt(num_MandiChicken.getText());
					double result = 10*quantity;
					txtpriceMandiChicken.setText(Double.toString(result));  
				}
			}
		
		});
		btnMandiChicken.setBounds(10, 170, 180, 23);
		panel_1.add(btnMandiChicken);
		
		JButton btnSpaghetti = new JButton("Spaghetti");
		btnSpaghetti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {					
				if (num_Spaghetti.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Spaghetti quantity is empty", "Error!", JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					int quantity = Integer.parseInt(num_Spaghetti.getText());
					double result = 6*quantity;
					txtpriceSpaghetti.setText(Double.toString(result));  
				}
			}
		});
		btnSpaghetti.setBounds(10, 211, 180, 23);
		panel_1.add(btnSpaghetti);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBounds(290, 57, 106, 256);
		contentPane.add(panel_2);
		
		JLabel lblPrice = new JLabel("Price (RM)");
		lblPrice.setBounds(21, 9, 75, 14);
		panel_2.add(lblPrice);
		
		txtpriceNasiLemak = new JTextField();
		txtpriceNasiLemak.setEditable(false);
		txtpriceNasiLemak.setText("0");
		txtpriceNasiLemak.setBounds(36, 49, 60, 20);
		panel_2.add(txtpriceNasiLemak);
		txtpriceNasiLemak.setColumns(10);
		
		JLabel lblRm = new JLabel("RM");
		lblRm.setBounds(10, 52, 30, 14);
		panel_2.add(lblRm);
		
		txtpriceChickenChop = new JTextField();
		txtpriceChickenChop.setEditable(false);
		txtpriceChickenChop.setText("0");
		txtpriceChickenChop.setColumns(10);
		txtpriceChickenChop.setBounds(36, 88, 60, 20);
		panel_2.add(txtpriceChickenChop);
		
		txtpriceFriedNoodles = new JTextField();
		txtpriceFriedNoodles.setEditable(false);
		txtpriceFriedNoodles.setText("0");
		txtpriceFriedNoodles.setColumns(10);
		txtpriceFriedNoodles.setBounds(36, 130, 60, 20);
		panel_2.add(txtpriceFriedNoodles);
		
		txtpriceMandiChicken = new JTextField();
		txtpriceMandiChicken.setEditable(false);
		txtpriceMandiChicken.setText("0");
		txtpriceMandiChicken.setColumns(10);
		txtpriceMandiChicken.setBounds(36, 171, 60, 20);
		panel_2.add(txtpriceMandiChicken);
		
		txtpriceSpaghetti = new JTextField();
		txtpriceSpaghetti.setEditable(false);
		txtpriceSpaghetti.setText("0");
		txtpriceSpaghetti.setColumns(10);
		txtpriceSpaghetti.setBounds(36, 211, 60, 20);
		panel_2.add(txtpriceSpaghetti);
		
		JLabel label = new JLabel("RM");
		label.setBounds(10, 91, 30, 14);
		panel_2.add(label);
		
		JLabel label_2 = new JLabel("RM");
		label_2.setBounds(10, 133, 30, 14);
		panel_2.add(label_2);
		
		JLabel label_3 = new JLabel("RM");
		label_3.setBounds(10, 174, 30, 14);
		panel_2.add(label_3);
		
		JLabel label_4 = new JLabel("RM");
		label_4.setBounds(10, 214, 30, 14);
		panel_2.add(label_4);
		
		JLabel lblTotalPrice = new JLabel("Total Price:");
		lblTotalPrice.setBounds(235, 334, 75, 14);
		contentPane.add(lblTotalPrice);
		
		JButton btnCalculateTotalOrder = new JButton("Calculate Total Order");
		btnCalculateTotalOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double quantity1 = Double.parseDouble(num_NasiLemak.getText());
				double quantity2 = Double.parseDouble(num_ChickenChop.getText());
				double quantity3 = Double.parseDouble(num_FriedNoodles.getText());
				double quantity4 = Double.parseDouble(num_MandiChicken.getText());
				double quantity5 = Double.parseDouble(num_Spaghetti.getText());
				
				OrderCalculation sum = new OrderCalculation();
				double totalprice = sum.plus(quantity1,quantity2,quantity3,quantity4,quantity5);
				
				OrderCalculation sumgst = new OrderCalculation();
				double totalpriceGST = sumgst.gst(totalprice,0.06);
				
				//double totalpriceGST = totalprice*1.06;
				txtpriceTotal.setText(String.format("%.2f",totalprice));				
				txtpriceTotalGST.setText(String.format("%.2f", totalpriceGST));
				txtNonmember.setText("Non-member");
		    }
		});
		btnCalculateTotalOrder.setBounds(10, 419, 386, 28);
		contentPane.add(btnCalculateTotalOrder);
		
		JButton btnSubmitOrder = new JButton("Submit Order");
		btnSubmitOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					if(new DBFoodOrder(). add(textTableNum.getText(),num_NasiLemak.getText(),num_ChickenChop.getText(),num_FriedNoodles.getText(),
							num_MandiChicken.getText(),num_Spaghetti.getText(),txtpriceTotal.getText(),txtpriceTotalGST.getText(),txtFoodStatus.getText(),txtPayStatus.getText()))
					{
						JOptionPane.showMessageDialog(null, "Order Submitted!");
						
						//Clear Text
						textTableNum.setText("");
						num_NasiLemak.setText("");
						num_ChickenChop.setText("");
						num_FriedNoodles.setText("");
						num_MandiChicken.setText("");
						num_Spaghetti.setText("");
						txtpriceTotal.setText("");
						txtpriceTotalGST.setText("");
						txtFoodStatus.setText("");
						txtPayStatus.setText("");
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Not Saved");
					}
			}
		});
		btnSubmitOrder.setBounds(10, 497, 386, 28);
		contentPane.add(btnSubmitOrder);
		
		JLabel label_6 = new JLabel("6%");
		label_6.setBounds(320, 359, 29, 14);
		contentPane.add(label_6);
		
		JLabel lblGst = new JLabel("GST:");
		lblGst.setBounds(235, 359, 75, 14);
		contentPane.add(lblGst);
		
		JLabel lblFinalPrice = new JLabel("Final Price:");
		lblFinalPrice.setBounds(235, 384, 75, 14);
		contentPane.add(lblFinalPrice);
		
		txtpriceTotal = new JTextField();
		txtpriceTotal.setText("0");
		txtpriceTotal.setEditable(false);
		txtpriceTotal.setColumns(10);
		txtpriceTotal.setBounds(320, 331, 60, 20);
		contentPane.add(txtpriceTotal);
		
		txtpriceTotalGST = new JTextField();
		txtpriceTotalGST.setText("0");
		txtpriceTotalGST.setEditable(false);
		txtpriceTotalGST.setColumns(10);
		txtpriceTotalGST.setBounds(320, 381, 60, 20);
		contentPane.add(txtpriceTotalGST);
		
		JLabel lblTableNum = new JLabel("Table Number");
		lblTableNum.setBounds(10, 21, 111, 14);
		contentPane.add(lblTableNum);
		
		textTableNum = new JTextField();
		textTableNum.setBounds(131, 18, 86, 20);
		contentPane.add(textTableNum);
		textTableNum.setColumns(10);
		
		JLabel lblFoodStatus = new JLabel("Food Status");
		lblFoodStatus.setBounds(10, 334, 94, 14);
		contentPane.add(lblFoodStatus);
		
		JLabel lblPayStatus = new JLabel("Pay Status");
		lblPayStatus.setBounds(10, 359, 94, 14);
		contentPane.add(lblPayStatus);
		
		txtFoodStatus = new JTextField();
		txtFoodStatus.setEditable(false);
		txtFoodStatus.setText("Pending");
		txtFoodStatus.setColumns(10);
		txtFoodStatus.setBounds(106, 331, 86, 20);
		contentPane.add(txtFoodStatus);
		
		txtPayStatus = new JTextField();
		txtPayStatus.setEditable(false);
		txtPayStatus.setText("Unpaid");
		txtPayStatus.setColumns(10);
		txtPayStatus.setBounds(106, 356, 86, 20);
		contentPane.add(txtPayStatus);
		
		JButton btnViewOrder = new JButton("View Order");
		btnViewOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                ChaletCook mgt = new ChaletCook();
                mgt.setVisible(true);
            	}
		});
		btnViewOrder.setBounds(10, 536, 386, 28);
		contentPane.add(btnViewOrder);
		
		JButton btnMembershipDiscount = new JButton("Membership Discount (10%)");
		btnMembershipDiscount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				   try
				    {
						double quantity1 = Double.parseDouble(num_NasiLemak.getText());
						double quantity2 = Double.parseDouble(num_ChickenChop.getText());
						double quantity3 = Double.parseDouble(num_FriedNoodles.getText());
						double quantity4 = Double.parseDouble(num_MandiChicken.getText());
						double quantity5 = Double.parseDouble(num_Spaghetti.getText());
						
						OrderCalculation sum = new OrderCalculation();
						double totalprice = sum.plus(quantity1,quantity2,quantity3,quantity4,quantity5);
						
						OrderCalculation member = new OrderCalculation();
						double totalpriceDiscount = member.membership(totalprice);
						
						OrderCalculation gst = new OrderCalculation();
						double totalpriceGST = gst.gst(totalpriceDiscount,0.06);
						
						txtpriceTotal.setText(String.format("%.2f", totalpriceDiscount));				
						txtpriceTotalGST.setText(String.format("%.2f", totalpriceGST));
						
				    }
				    catch(NumberFormatException ex)
				    {
						JOptionPane.showMessageDialog(null, "Invalid price", "Error!", JOptionPane.ERROR_MESSAGE);
				    }
				   txtNonmember.setText("Registered");
						
			}
		});
		btnMembershipDiscount.setBounds(10, 458, 386, 28);
		contentPane.add(btnMembershipDiscount);
		
		JLabel lblMembership = new JLabel("Membership");
		lblMembership.setBounds(10, 384, 94, 14);
		contentPane.add(lblMembership);
		
		txtNonmember = new JTextField();
		txtNonmember.setText("Non-member");
		txtNonmember.setEditable(false);
		txtNonmember.setColumns(10);
		txtNonmember.setBounds(106, 381, 86, 20);
		contentPane.add(txtNonmember);
	}
}
