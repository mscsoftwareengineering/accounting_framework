package system;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import database.DBTableReserve;
import main.Option;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Chalet Count User Interface
 * 
 * @author MohamadRaziman
 *
 */
public class ChaletCount extends JFrame {

	private JPanel contentPane;
	private JTextField cust_nameTxt;
	private JTextField cust_phoneTxt;
	private JTextField table_numTxt;
	private JTextField timeTxt;
	private JTextField no_personTxt;
	private JTable jTable1;
	
	public void close(){

		WindowEvent winClosingEvent = new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);

		}

	/**
	 * 
	 * Launch the application.
	 * @param args passing arguments
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChaletCount frame = new ChaletCount();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Reserve table information
	 * @param evt passing Mouse Event 
	 */
	public void jTable1MouseClicked(java.awt.event.MouseEvent evt){
		String cust_name = jTable1.getValueAt(jTable1.getSelectedRow(), 1).toString();
		String cust_phone = jTable1.getValueAt(jTable1.getSelectedRow(), 2).toString();
		String table_num = jTable1.getValueAt(jTable1.getSelectedRow(), 3).toString();
		String time = jTable1.getValueAt(jTable1.getSelectedRow(), 4).toString();
		String no_person = jTable1.getValueAt(jTable1.getSelectedRow(), 5).toString();
		cust_nameTxt.setText(cust_name);
		cust_phoneTxt.setText(cust_phone);
		table_numTxt.setText(table_num);
		timeTxt.setText(time);
		no_personTxt.setText(no_person);
	}
	
	/**
	 * Connection to Database
	 * @param query Query
	 */
	public void theQuery(String query){
		Connection con = null;
		Statement st = null;
		try{
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/restaurant", "root", "");
			st = con.createStatement();
			st.executeUpdate(query);
			JOptionPane.showMessageDialog(null,"Query Executed");
		}
		catch(Exception ex){
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
	}

	/**
	 * Create the frame.
	 */
	public ChaletCount() {
		setTitle("Chalet Count");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 864, 562);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMainMenu = new JMenu("Main Menu");
		menuBar.add(mnMainMenu);
		
		JMenuItem mntmSelectSystem = new JMenuItem("Select System");
		mntmSelectSystem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Option back = new Option();
				back.setVisible(true);
			}
		});
		mnMainMenu.add(mntmSelectSystem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Reserve Table");
		label.setBounds(10, 25, 104, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Name:");
		label_1.setBounds(10, 50, 123, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Phone Number:");
		label_2.setBounds(10, 75, 123, 14);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("Reservation Details");
		label_3.setBounds(10, 113, 123, 14);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("Table Number:");
		label_4.setBounds(10, 138, 86, 14);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("Time:");
		label_5.setBounds(10, 163, 104, 14);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("No. of Person(s):");
		label_6.setBounds(10, 188, 104, 14);
		contentPane.add(label_6);
		
		cust_nameTxt = new JTextField();
		cust_nameTxt.setColumns(10);
		cust_nameTxt.setBounds(122, 47, 196, 20);
		contentPane.add(cust_nameTxt);
		
		cust_phoneTxt = new JTextField();
		cust_phoneTxt.setColumns(10);
		cust_phoneTxt.setBounds(122, 75, 196, 20);
		contentPane.add(cust_phoneTxt);
		
		table_numTxt = new JTextField();
		table_numTxt.setColumns(10);
		table_numTxt.setBounds(122, 135, 196, 20);
		contentPane.add(table_numTxt);
		
		timeTxt = new JTextField();
		timeTxt.setColumns(10);
		timeTxt.setBounds(122, 160, 196, 20);
		contentPane.add(timeTxt);
		
		no_personTxt = new JTextField();
		no_personTxt.setColumns(10);
		no_personTxt.setBounds(122, 185, 196, 20);
		contentPane.add(no_personTxt);
		
		JButton btnNewButton = new JButton("Reserve Table");
		btnNewButton.addActionListener(new ActionListener() {
		/**
			* Reserve button function
			* @param Customer details
			* @return Store customer details to database
			*/
			public void actionPerformed(ActionEvent e) 
			{
				
					if(new DBTableReserve(). add(cust_nameTxt.getText(),cust_phoneTxt.getText(),table_numTxt.getText(),timeTxt.getText(),no_personTxt.getText()))
					{
						JOptionPane.showMessageDialog(null, "Table Reserved");
						
						//Clear Text
						cust_nameTxt.setText("");
						cust_phoneTxt.setText("");
						table_numTxt.setText("");
						timeTxt.setText("");
						no_personTxt.setText("");
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Not Saved");
					}
						
									
					
			}
		});
		btnNewButton.setBounds(25, 243, 293, 32);
		contentPane.add(btnNewButton);
		
		JButton btnUpdateTable = new JButton("Update Table");
		btnUpdateTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=jTable1.getSelectedRow();
				String id=jTable1.getValueAt(index, 0).toString();
				
				if(new DBTableReserve().update(id, cust_nameTxt.getText(),cust_phoneTxt.getText(),table_numTxt.getText(),timeTxt.getText(),no_personTxt.getText()))
				{
					JOptionPane.showMessageDialog(null, "Reservation Updated!");
					
					//Clear Text
					cust_nameTxt.setText("");
					cust_phoneTxt.setText("");
					table_numTxt.setText("");
					timeTxt.setText("");
					no_personTxt.setText("");
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Update failed");
				}
			}
		});
		btnUpdateTable.setBounds(25, 286, 293, 32);
		contentPane.add(btnUpdateTable);
		
		JButton btnDeleteTable = new JButton("Delete Table");
		btnDeleteTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] options={"Yes","No"};
				int answ = JOptionPane.showOptionDialog(null, "Sure To Delete??", "Delete Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
				
				if(answ==0)
				{
					int index=jTable1.getSelectedRow();
					String id=jTable1.getValueAt(index, 0).toString();
					
					if(new DBTableReserve().delete(id))
					{
						JOptionPane.showMessageDialog(null, "Reservation Deleted!");
						
						//Clear Text
						cust_nameTxt.setText("");
						cust_phoneTxt.setText("");
						table_numTxt.setText("");
						timeTxt.setText("");
						no_personTxt.setText("");
						
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Delete failed");
					}
					
				}
			}
		});
		btnDeleteTable.setBounds(25, 329, 293, 32);
		contentPane.add(btnDeleteTable);
		
		JButton btnRetrieveData = new JButton("Refresh Database");
		btnRetrieveData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel dm = new DBTableReserve().getData();
				jTable1.setModel(dm);
			}
		});
		btnRetrieveData.setBounds(25, 372, 293, 32);
		contentPane.add(btnRetrieveData);
		
		JButton btnClearForm = new JButton("Clear Form");
		btnClearForm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{				
				cust_nameTxt.setText("");
				cust_phoneTxt.setText("");
				table_numTxt.setText("");
				timeTxt.setText("");
				no_personTxt.setText("");
				}
		});
		btnClearForm.setBounds(25, 415, 293, 32);
		contentPane.add(btnClearForm);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {        	
			Option back = new Option();
			back.setVisible(true);
			
			}
		});
		btnBack.setBounds(25, 458, 293, 32);
		contentPane.add(btnBack);
		DefaultTableModel dm = new DBTableReserve().getData();
	
		
		JLabel label_7 = new JLabel("List of Reserved Table");
		label_7.setBounds(342, 25, 167, 14);
		contentPane.add(label_7);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(342, 50, 496, 440);
		contentPane.add(scrollPane);
		
		jTable1 = new JTable();
		scrollPane.setViewportView(jTable1);
		jTable1.setModel(dm);
	}
}
