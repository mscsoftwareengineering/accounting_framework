package system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import calculation.OrderCalculation;
import database.DBStorage;
import main.Option;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;

/**
 * Chalet Management User Interface
 * 
 * @author MohamadRaziman
 *
 */
public class ChaletManagement extends JFrame {

	private JPanel contentPane;
	private JTextField storageItem;
	private JTextField storagePrice;
	private JTextField storageQuantity;
	private JTextField storageBudget;
	private JTable jTable1;
	private JTextField storageTotalPrice;
	private JTextField storageBudgetLeft;
	private JTextField storageMonth;

	/**
	 * Launch the application.
	 * @param args Arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChaletManagement frame = new ChaletManagement();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Connection to Database
	 * @param query Query
	 */
	public void theQuery(String query){
		Connection con = null;
		Statement st = null;
		try{
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/restaurant", "root", "");
			st = con.createStatement();
			st.executeUpdate(query);
			JOptionPane.showMessageDialog(null,"Query Executed");
		}
		catch(Exception ex){
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
	}

	/**
	 * Create the frame.
	 */
	public ChaletManagement() {
		setTitle("Chalet Restaurant Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 956, 558);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMainMenu = new JMenu("Main Menu");
		menuBar.add(mnMainMenu);
		
		JMenuItem mntmHome = new JMenuItem("Select System");
		mntmHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Option back = new Option();
				back.setVisible(true);
			}
		});
		mnMainMenu.add(mntmHome);
		
		JMenu mnChaletTable = new JMenu("Chalet Count");
		menuBar.add(mnChaletTable);
		
		JMenuItem mntmNew = new JMenuItem("Table Reservation");
		mnChaletTable.add(mntmNew);
		
		JMenu mnChaletManagement = new JMenu("Chalet Management");
		menuBar.add(mnChaletManagement);
		
		JMenuItem mntmStorageManagement = new JMenuItem("Storage Management");
		mnChaletManagement.add(mntmStorageManagement);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(10, 11, 317, 463);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblBudgetCalculator = new JLabel("Storage Management");
		lblBudgetCalculator.setBounds(106, 11, 129, 14);
		panel_1.add(lblBudgetCalculator);
		
		JLabel lblItem = new JLabel("Item:");
		lblItem.setBounds(10, 40, 46, 14);
		panel_1.add(lblItem);
		
		storageItem = new JTextField();
		storageItem.setBounds(152, 36, 155, 20);
		panel_1.add(storageItem);
		storageItem.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(10, 70, 46, 14);
		panel_1.add(lblPrice);
		
		storagePrice = new JTextField();
		storagePrice.setColumns(10);
		storagePrice.setBounds(152, 67, 155, 20);
		panel_1.add(storagePrice);
		
		storageQuantity = new JTextField();
		storageQuantity.setColumns(10);
		storageQuantity.setBounds(152, 98, 155, 20);
		panel_1.add(storageQuantity);
		
		JLabel lblQuantity = new JLabel("Quantity (Unit):");
		lblQuantity.setBounds(10, 101, 99, 14);
		panel_1.add(lblQuantity);
		
		JLabel lblBudget_1 = new JLabel("Current Budget:");
		lblBudget_1.setBounds(10, 178, 99, 14);
		panel_1.add(lblBudget_1);
		
		storageBudget = new JTextField();
		storageBudget.setBounds(152, 175, 155, 20);
		panel_1.add(storageBudget);
		storageBudget.setColumns(10);
		
		JButton btnSubmit = new JButton("Refresh Report");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel dm = new DBStorage().getData();
				jTable1.setModel(dm);
			}
		});
		btnSubmit.setBounds(10, 385, 297, 23);
		panel_1.add(btnSubmit);
		
		JButton btnCalculate = new JButton("Total Price");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				   try
				    {
						double price = Double.parseDouble(storagePrice.getText());
						double quantity = Double.parseDouble(storageQuantity.getText());
						
						OrderCalculation total = new OrderCalculation();
						double totalprice = total.multiply(price,quantity);
	
						storageTotalPrice.setText(String.format("%.2f", totalprice));
				    }
				    catch(NumberFormatException ex)
				    {
						JOptionPane.showMessageDialog(null, "Invalid price", "Error!", JOptionPane.ERROR_MESSAGE);
				    } 
			}
		});
		btnCalculate.setBounds(10, 132, 297, 23);
		panel_1.add(btnCalculate);
		
		JButton btnSaveReport = new JButton("Save Report");
		btnSaveReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				/**
				 * Reserve button function
				 * @param Customer details
				 * @return Store customer details to database
				 */
					
					if(new DBStorage(). add(storageItem.getText(),storagePrice.getText(),storageQuantity.getText(),storageBudgetLeft.getText(),storageTotalPrice.getText(),storageMonth.getText()))
					{
						JOptionPane.showMessageDialog(null, "Report saved!");
						
						//Clear Text
						storageItem.setText("");
						storagePrice.setText("");
						storageQuantity.setText("");
						storageBudgetLeft.setText("");
						storageTotalPrice.setText("");
						storageMonth.setText("");
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Not Saved");
					}
					
								
				
			}
		});
		btnSaveReport.setBounds(10, 351, 296, 23);
		panel_1.add(btnSaveReport);
		
		JButton btnRemainingBudget = new JButton("Remaining Budget");
		btnRemainingBudget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				   try
				    {
					   
					   
						double totalprice = Double.parseDouble(storageTotalPrice.getText());
						double budget = Double.parseDouble(storageBudget.getText());
						
						OrderCalculation total = new OrderCalculation();
						double result = total.storage(totalprice,budget);
						
						storageBudgetLeft.setText(String.format("%.2f", result));
				    }
				    catch(NumberFormatException ex)
				    {
						JOptionPane.showMessageDialog(null, "Invalid price", "Error!", JOptionPane.ERROR_MESSAGE);
				    } 
			}
		});
		btnRemainingBudget.setBounds(10, 247, 297, 23);
		panel_1.add(btnRemainingBudget);
		
		JLabel lblTotalPrice = new JLabel("Total Price:");
		lblTotalPrice.setBounds(10, 213, 77, 14);
		panel_1.add(lblTotalPrice);
		
		storageTotalPrice = new JTextField();
		storageTotalPrice.setText("0");
		storageTotalPrice.setEditable(false);
		storageTotalPrice.setColumns(10);
		storageTotalPrice.setBounds(152, 210, 155, 20);
		panel_1.add(storageTotalPrice);
		
		JLabel lblRemainingBudget = new JLabel("Remaining Budget:");
		lblRemainingBudget.setBounds(10, 284, 118, 14);
		panel_1.add(lblRemainingBudget);
		
		storageBudgetLeft = new JTextField();
		storageBudgetLeft.setText("0");
		storageBudgetLeft.setEditable(false);
		storageBudgetLeft.setColumns(10);
		storageBudgetLeft.setBounds(152, 281, 155, 20);
		panel_1.add(storageBudgetLeft);
		
		JLabel lblDate = new JLabel("Report (Month):");
		lblDate.setBounds(10, 312, 118, 14);
		panel_1.add(lblDate);
		
		storageMonth = new JTextField();
		storageMonth.setColumns(10);
		storageMonth.setBounds(152, 309, 155, 20);
		panel_1.add(storageMonth);
		
		JButton btnDeleteReport = new JButton("Delete Report");
		btnDeleteReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] options={"Yes","No"};
				int answ = JOptionPane.showOptionDialog(null, "Delete report?", "Delete Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
				
				if(answ==0)
				{
					int index=jTable1.getSelectedRow();
					String id=jTable1.getValueAt(index, 0).toString();
					
					if(new DBStorage().delete(id))
					{
						JOptionPane.showMessageDialog(null, "Report Deleted!");
						
						//Clear Text
						storageItem.setText("");
						storagePrice.setText("");
						storageQuantity.setText("");
						storageBudget.setText("");
						storageTotalPrice.setText("");
						storageMonth.setText("");
						
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Delete failed");
					}
					
				}
			}
		});
		btnDeleteReport.setBounds(10, 419, 297, 23);
		panel_1.add(btnDeleteReport);
		
		JLabel label_2 = new JLabel("RM");
		label_2.setBounds(119, 284, 22, 14);
		panel_1.add(label_2);
		
		JLabel label = new JLabel("RM");
		label.setBounds(120, 213, 22, 14);
		panel_1.add(label);
		
		JLabel label_1 = new JLabel("RM");
		label_1.setBounds(120, 178, 22, 14);
		panel_1.add(label_1);
		
		JLabel label_3 = new JLabel("RM");
		label_3.setBounds(120, 70, 22, 14);
		panel_1.add(label_3);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(337, 11, 593, 463);
		contentPane.add(scrollPane);
		
		jTable1 = new JTable();
		scrollPane.setViewportView(jTable1);
		DefaultTableModel dm = new DBStorage().getData();
		jTable1.setModel(dm);
	
	}
}
