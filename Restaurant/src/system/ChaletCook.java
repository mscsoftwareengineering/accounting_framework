package system;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import database.DBFoodOrder;
import main.Option;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Chalet Cook User Interface
 * 
 * @author MohamadRaziman
 *
 */
public class ChaletCook extends JFrame {

	private JPanel contentPane;
	private JTable jTable1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChaletCook frame = new ChaletCook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
    /**
     * Functions to open other system and close the portal
     */
    
	public void close(){

		WindowEvent winClosingEvent = new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);

		}

	/**
	 * Create the frame.
	 */
	public ChaletCook() {
		setTitle("Chalet Cook");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1120, 626);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMainMenu = new JMenu("Main Menu");
		menuBar.add(mnMainMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Select System");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Option back = new Option();
				back.setVisible(true);
			}
		});
		mnMainMenu.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("List of Order");
		lblNewLabel.setBounds(213, 11, 98, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblOrderManagement = new JLabel("Order Status Management");
		lblOrderManagement.setBounds(10, 11, 215, 14);
		contentPane.add(lblOrderManagement);
		
		JButton btnCookOrder = new JButton("Cooking");
		btnCookOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=jTable1.getSelectedRow();
				String id=jTable1.getValueAt(index, 0).toString();
				
				if(new DBFoodOrder().cookOrder(id, btnCookOrder.getText()))
				{
					JOptionPane.showMessageDialog(null, "Preparing the order");
					
				//Clear Text
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Update failed");
				}
			}
		});
		btnCookOrder.setBounds(10, 120, 193, 76);
		contentPane.add(btnCookOrder);
		
		JButton btnCancelOrder = new JButton("Cancel Order");
		btnCancelOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] options={"Yes","No"};
				int answ = JOptionPane.showOptionDialog(null, "Cancel Order?", "Delete Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
				
				if(answ==0)
				{
					int index=jTable1.getSelectedRow();
					String id=jTable1.getValueAt(index, 0).toString();
					
					if(new DBFoodOrder().delete(id))
					{
						JOptionPane.showMessageDialog(null, "Order Deleted!");
						
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Delete failed");
					}
					
				}
			}
		});
		btnCancelOrder.setBounds(10, 207, 193, 76);
		contentPane.add(btnCancelOrder);
		
		JButton btnCompleteOrder = new JButton("Completed");
		btnCompleteOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=jTable1.getSelectedRow();
				String id=jTable1.getValueAt(index, 0).toString();
				
				if(new DBFoodOrder().updateOrder(id, btnCompleteOrder.getText()))
				{
					JOptionPane.showMessageDialog(null, "Order Completed!");
					
				//Clear Text
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Update failed");
				}
			}
		});
		btnCompleteOrder.setBounds(10, 294, 193, 76);
		contentPane.add(btnCompleteOrder);
		
		JButton btnRefreshOrder = new JButton("Refresh Order");
		btnRefreshOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel dm = new DBFoodOrder().getData();
				jTable1.setModel(dm);
			}
		});
		btnRefreshOrder.setBounds(10, 36, 193, 76);
		contentPane.add(btnRefreshOrder);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(213, 36, 881, 508);
		contentPane.add(scrollPane);
		
		jTable1 = new JTable();
		scrollPane.setViewportView(jTable1);
		DefaultTableModel dm = new DBFoodOrder().getData();
		jTable1.setModel(dm);
		
		JButton btnPaid = new JButton("Paid");
		btnPaid.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int index=jTable1.getSelectedRow();
					String id=jTable1.getValueAt(index, 0).toString();
					
					if(new DBFoodOrder().orderPaid(id, btnPaid.getText()))
					{
						JOptionPane.showMessageDialog(null, "Order Paid!");
						
					//Clear Text
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Update failed");
					}
				
			}
		});
		btnPaid.setBounds(10, 381, 193, 76);
		contentPane.add(btnPaid);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	        	Option back = new Option();
	        	back.setVisible(true);			
			}
		});
		btnBack.setBounds(10, 468, 193, 76);
		contentPane.add(btnBack);
	}
}
