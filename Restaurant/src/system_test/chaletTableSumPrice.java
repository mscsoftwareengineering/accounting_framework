package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletTableSumPrice {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double totalOrder = test.plus(4,2,5,2,1);
		assertEquals(64.50, totalOrder, 10.0);
	}

}