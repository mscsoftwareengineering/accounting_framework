package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletManagementMultiply {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double budget = test.multiply(10,100);
		assertEquals(1000, budget, 0.0);
	}

}
