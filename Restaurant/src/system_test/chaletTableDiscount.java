package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletTableDiscount {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double totalOrder = test.membership(30.21);
		assertEquals(27.19, totalOrder, 10);
	}

}