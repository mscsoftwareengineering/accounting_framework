package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletTableGST {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double totalOrder = test.gst(28.5,0.06);
		assertEquals(30.21, totalOrder, 10);
	}

}