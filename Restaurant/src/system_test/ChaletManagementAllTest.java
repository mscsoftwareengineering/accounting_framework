package system_test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ chaletManagementBudget.class, 
	chaletManagementMultiply.class })
public class ChaletManagementAllTest {

}
