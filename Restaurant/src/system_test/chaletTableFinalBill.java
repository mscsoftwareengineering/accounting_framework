package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletTableFinalBill {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double discountprice = test.membership(28.5);
		assertEquals(25.65, discountprice, 10);
		
		double gstprice = test.gst(25.65,0.06);
		assertEquals(27.19, gstprice, 10);	
		
	}

}