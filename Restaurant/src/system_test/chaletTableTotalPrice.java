package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletTableTotalPrice {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double totalOrder = test.multiply(2,4);
		assertEquals(8, totalOrder, 0.0);
	}

}