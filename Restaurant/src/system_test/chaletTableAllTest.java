package system_test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	chaletTableDiscount.class, 
	chaletTableFinalBill.class, 
	chaletTableGST.class, 
	chaletTableSumPrice.class,
	chaletTableTotalPrice.class })
public class chaletTableAllTest {
}
