package system_test;

import static org.junit.Assert.*;

import org.junit.Test;

import calculation.OrderCalculation;

public class chaletManagementBudget {

	@Test
	public void test() {
		OrderCalculation test = new OrderCalculation();
		double budget = test.storage(1000,50000);
		assertEquals(49000, budget, 0.0);
	}
}
