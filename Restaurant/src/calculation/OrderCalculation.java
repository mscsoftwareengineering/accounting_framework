/**
 * @author MohamadRaziman
 */

package calculation;

public class OrderCalculation {
	
	//4 calculation methods for Chalet Tablet System
	//1 calculation method for Chalet Management System
	
	   /**
	   * @return sum of price
	   * Function used on Chalet Table system to sum up all 5 orders
	   */
	public double plus(double quantity1, double quantity2, double quantity3, double quantity4, double quantity5)
	{
		double sumofPrice = 0;
		double menu1 = quantity1*1.5;
		double menu2 = quantity2*7.5;
		double menu3 = quantity3*3.5;
		double menu4 = quantity4*10.0;
		double menu5 = quantity5*6.0;
		sumofPrice = menu1 + menu2 + menu3 + menu4 + menu5;
		return sumofPrice ;
	}
	
	   /**
	   * @return total price according to quantity
	   * Function used on Chalet Management to calculate total price to estimate budget
	   */
	public double multiply(double price, double quantity)
	{
		double totalPrice = 0;
		totalPrice =  price * quantity;
		return totalPrice;
	}
	
	   /**
	   * @return total price including GST
	   * Function used on Chalet Table system to calculate price after GST
	   */
	
	public double gst(double price, double gst)
	{
		double totalPriceGST = 0;
		totalPriceGST = price - (price*gst);
		return totalPriceGST;
	}
	
	   /**
	   * @return total price after discount
	   * Function used on Chalet Table system to calculate price after discount
	   */
	
	public double membership(double totalprice)
	{
		double totalPriceMembership = 0;
		totalPriceMembership = totalprice*0.9;
		return totalPriceMembership;
	}
	
	   /**
	   * @return monthly budget
	   * Function used to calculate monthly budget for Chalet management system
	   */
	
	public double storage(double totalPrice, double budget)
	{
		double budgetLeft = 0;
		budgetLeft = budget - totalPrice;
		return budgetLeft;
	}
	
}
