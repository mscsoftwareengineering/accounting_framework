package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Font;

/**
 * Logging UserInteface
 * 
 * @author MohamadRaziman
 *
 */
public class Login {

	private JFrame frmLogin;
	private JTextField txtJtxtusername;
	private JPasswordField passwordField;
	private Option winOption;

	/**
	 * Launch the application.
	 * @param args Arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		winOption = new Option();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setTitle("Login");
		frmLogin.setBounds(100, 100, 450, 299);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		JLabel lblLogin = new JLabel("Login to System:");
		lblLogin.setBounds(26, 76, 127, 14);
		frmLogin.getContentPane().add(lblLogin);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(26, 116, 87, 14);
		frmLogin.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(26, 158, 87, 14);
		frmLogin.getContentPane().add(lblPassword);
		
		txtJtxtusername = new JTextField();
		txtJtxtusername.setBounds(114, 113, 288, 20);
		frmLogin.getContentPane().add(txtJtxtusername);
		txtJtxtusername.setColumns(10);
		
		
		//Login System
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String password = passwordField.getText();
				String username = txtJtxtusername.getText();
				
				if (password.contains("pass") && (username.contains("admin")))
						{
							passwordField.setText(null);
							txtJtxtusername.setText(null);
							winOption.setVisible(true);
							
						}
				else
				{
					JOptionPane.showMessageDialog(null, "Invalid login details", "Error!", JOptionPane.ERROR_MESSAGE);
					passwordField.setText(null);
					txtJtxtusername.setText(null);
				}
					
				
			}
		});
		btnLogin.setBounds(306, 203, 89, 23);
		frmLogin.getContentPane().add(btnLogin);
		
		//Clear the form
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				passwordField.setText(null);
				txtJtxtusername.setText(null);
			}
		});
		btnClear.setBounds(209, 203, 89, 23);
		frmLogin.getContentPane().add(btnClear);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(114, 155, 281, 20);
		frmLogin.getContentPane().add(passwordField);
		
		JButton btnClose = new JButton("Close");
		
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnClose.setBounds(114, 203, 89, 23);
		frmLogin.getContentPane().add(btnClose);
		
		JLabel lblWelcomeToChalet = new JLabel("Welcome to Chalet Restaurant System");
		lblWelcomeToChalet.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblWelcomeToChalet.setBounds(48, 22, 357, 28);
		frmLogin.getContentPane().add(lblWelcomeToChalet);
	}
}
