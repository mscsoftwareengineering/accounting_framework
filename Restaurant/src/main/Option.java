package main;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import system.ChaletCook;
import system.ChaletCount;
import system.ChaletManagement;
import system.ChaletRun;
import system.ChaletTable;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

/**
 * Interface after Login
 * 
 * @author MohamadRaziman
 *
 */
public class Option extends JFrame {

    private JPanel contentPane;

    /**
     * Launch the application - Options for all systems
     * @param args Arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Option frame = new Option();
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
	/**
	 * Functions to open other system and close the portal
	 */
	public void close(){

		WindowEvent winClosingEvent = new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);

		}

    /**
     * Create the option frame.
     */
    public Option() {
    	setTitle("Select System");
        setBounds(100, 100, 272, 384);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblNewLabel = new JLabel("Select System :");
        lblNewLabel.setBounds(10, 11, 93, 14);
        contentPane.add(lblNewLabel);
         
        JButton btnNewButton = new JButton("Chalet Count");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	ChaletCount table = new ChaletCount();
        	table.setVisible(true);
        	close();
        	}
        });
        
        btnNewButton.setBounds(20, 36, 217, 43);
        contentPane.add(btnNewButton);
        
        //Open Chalet Run New windows 
        JButton btnChaletRun = new JButton("Chalet Run");
        btnChaletRun.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		ChaletRun mgt = new ChaletRun();
                mgt.setVisible(true);
        		close();
        	}
        });
        btnChaletRun.setBounds(20, 89, 217, 43);
        contentPane.add(btnChaletRun);
        
        //Open Chalet Table New windows 
        JButton btnChaletCount = new JButton("Chalet Table");
        btnChaletCount.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		ChaletTable mgt = new ChaletTable();
                mgt.setVisible(true);
                close();
        	}
        });
        btnChaletCount.setBounds(20, 143, 217, 43);
        contentPane.add(btnChaletCount);
        
        //Open Chalet Cook New windows 
        JButton btnChaletCook = new JButton("Chalet Cook");
        btnChaletCook.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
                ChaletCook mgt = new ChaletCook();
                mgt.setVisible(true);
                close();
            	}
        });
        btnChaletCook.setBounds(20, 197, 217, 43);
        contentPane.add(btnChaletCook);
        
        //Open Management Count New windows 
        JButton btnChaletManagement = new JButton("Chalet Management");
        btnChaletManagement.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
            ChaletManagement mgt = new ChaletManagement();
            mgt.setVisible(true);
            close();
        	}
        });
        btnChaletManagement.setBounds(20, 251, 217, 43);
        contentPane.add(btnChaletManagement);
        
        JButton btnBack = new JButton("Close");
        btnBack.setBounds(20, 305, 102, 23);
        contentPane.add(btnBack);
        
        JButton btnBack_1 = new JButton("Back");
        btnBack_1.setBounds(132, 305, 105, 23);
        contentPane.add(btnBack_1);
    }
}
